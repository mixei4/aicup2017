using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
	public sealed class MyStrategy : IStrategy
	{
		public static World world;
		private int groupMoveInterval = 200;
		private int stepAir = 1;
		private int stepGround = 1;
		private int groupStep = 45;
		Point fightersPoint;
		Point helicoptersPoint;
		private string spreadType = "";
		private Player me;
		private Player opponent;
		private Game game;
		private Move move;
		private Random rnd;
		private Dictionary<long, Vehicle> vehicleById = new Dictionary<long, Vehicle>();
		private Dictionary<long, Vehicle> prevVehicleById;
		private List<Vehicle> myVehicles, myVehiclesLand, hisVehicles, hisVehiclesLand, myArmy;
		private WeatherType[][] weather;
		private TerrainType[][] terrain;
		private Point armyCenter;
		private List<Tuple<int, Move>> movesHistory = new List<Tuple<int, Move>>();
		private MovesList moves = new MovesList();
		private Dictionary<VehicleType, VehiclesGroup> vehiclesByType, opponentVehiclesByType;
		private Dictionary<int, VehiclesGroup> myGroups = new Dictionary<int, VehiclesGroup>();
		private VehicleType[] moveOrder = new VehicleType[3] { VehicleType.Tank, VehicleType.Ifv, VehicleType.Arrv };
		private Dictionary<VehicleType, bool> IsMovedByType;
		private Dictionary<VehicleType, bool> IsFightByType;
		private Dictionary<VehicleType, bool> IsSpreadedByType = new Dictionary<VehicleType, bool>();
		private Dictionary<string, int> tickHistory = new Dictionary<string, int>();
		private Point[] corners = new Point[4];
		private Behaviour fighterBehaviour = new Behaviour();
		private bool isFacilities;
		private Facility[] facilities, myFacilities, notMyFacilities, hisFacilities, neutralFacilities;
		private Dictionary<long, FacilityAdd> facilitiesAdd = new Dictionary<long, FacilityAdd>();
		private const int firstGroupId = Group.Count + 1;
		private int lastGroupId = firstGroupId - 1;
		private NuclearPoint bestNuclearPoint;
		private int nuclearGroupId;
		private int lastNuclearStrikeAttempt;
		private int lastNuclearStrikeAvoid;
		private int lastNuclearStrikeAvoidGroupId;
		private bool isAllSeparate = false;
		private bool isTankSeparate = false;
		private string tankGoDir = "";
		private int hisHelicoptersTickCount = 0;
#if DEBUG
		private VisualClient vc = new VisualClient("127.0.0.1", 13579);
#endif

		static MyStrategy()
		{

		}

		private int NextGroupId
		{
			get {
				lastGroupId++;
				return lastGroupId;
			}
		}

		private void Init(Player _me, World _world, Game _game, Move _move)
		{
			me = _me;
			world = _world;
			game = _game;
			move = _move;
			if (world.TickIndex == 0)
			{
				isFacilities = (world.Facilities.Count() > 0);
				foreach (var facility in world.Facilities)
				{
					facilitiesAdd.Add(facility.Id, new FacilityAdd());
				}
			}
			opponent = world.Players.Where(q => !q.IsMe).First();
			vehiclesByType = new Dictionary<VehicleType, VehiclesGroup>();
			opponentVehiclesByType = new Dictionary<VehicleType, VehiclesGroup>();
			prevVehicleById = new Dictionary<long, Vehicle>();
			weather = world.WeatherByCellXY;
			terrain = world.TerrainByCellXY;
			if (isFacilities)
			{
				facilities = world.Facilities;
				myFacilities = facilities.Where(q => q.OwnerPlayerId == me.Id).ToArray();
				notMyFacilities = facilities.Where(q => q.OwnerPlayerId != me.Id).ToArray();
				hisFacilities = facilities.Where(q => q.OwnerPlayerId == opponent.Id).ToArray();
				neutralFacilities = facilities.Where(q => q.OwnerPlayerId == -1).ToArray();
				foreach (var facility in notMyFacilities)
				{
					facilitiesAdd[facility.Id] = new FacilityAdd();
				}
				groupMoveInterval = Math.Max(200, 30 * myGroups.Count());
			}
			if (world.TickIndex > 0)
			{
				foreach (var vehicle in vehicleById)
				{
					prevVehicleById.Add(vehicle.Key, vehicle.Value);
				}
			}
			foreach (Vehicle vehicle in world.NewVehicles)
			{
				vehicleById.Add(vehicle.Id, vehicle);
			}
			IsMovedByType = new Dictionary<VehicleType, bool>();
			IsFightByType = new Dictionary<VehicleType, bool>();
			foreach (VehicleType vehicleType in Enum.GetValues(typeof(VehicleType)))
			{
				IsMovedByType.Add(vehicleType, false);
				IsFightByType.Add(vehicleType, false);
			}
			foreach (VehicleUpdate vehicleUpdate in world.VehicleUpdates)
			{
				long vehicleId = vehicleUpdate.Id;
				if (vehicleUpdate.Durability == 0)
				{
					if (vehicleById[vehicleId].PlayerId == me.Id)
					{
						IsFightByType[vehicleById[vehicleId].Type] = true;
					}
					vehicleById.Remove(vehicleId);
				}
				else
				{
					if (vehicleById[vehicleId].PlayerId == me.Id)
					{
						if (!vehicleById[vehicleId].X.Eq(vehicleUpdate.X) || !vehicleById[vehicleId].Y.Eq(vehicleUpdate.Y))
						{
							IsMovedByType[vehicleById[vehicleId].Type] = true;
						}
						if (vehicleUpdate.Durability < vehicleById[vehicleId].Durability)
						{
							IsFightByType[vehicleById[vehicleId].Type] = true;
						}
					}
					vehicleById[vehicleId] = new Vehicle(vehicleById[vehicleId], vehicleUpdate);
				}
			}
			myVehicles = vehicleById.Select(q => q.Value).Where(q => (q.PlayerId == me.Id)).ToList();
			myVehiclesLand = myVehicles.Where(q => !q.IsAerial).ToList();
			hisVehicles = vehicleById.Select(q => q.Value).Where(q => (q.PlayerId != me.Id)).ToList();
			hisVehiclesLand = hisVehicles.Where(q => !q.IsAerial).ToList();
			myArmy = myVehicles.Where(q => Group.ARMY.In(q.Groups)).ToList();
			if (myArmy.Count() > 0)
			{
				armyCenter = new Point(myArmy.Select(q => q.X).Average(), myArmy.Select(q => q.Y).Average());
			}
			foreach (VehicleType vehicleType in Enum.GetValues(typeof(VehicleType)))
			{
				vehiclesByType.Add(vehicleType, new VehiclesGroup(myVehicles.Where(q => q.Type == vehicleType).ToList()));
				opponentVehiclesByType.Add(vehicleType, new VehiclesGroup(hisVehicles.Where(q => q.Type == vehicleType).ToList()));
			}
			if (opponentVehiclesByType[VehicleType.Helicopter].Count() > 0)
			{
				hisHelicoptersTickCount++;
			}
			if (lastGroupId >= firstGroupId)
			{
				for (var groupId = firstGroupId; groupId <= lastGroupId; groupId++)
				{
					var units = myVehicles.Where(q => groupId.In(q.Groups));
					if (units.Count() > 0)
					{
						if (myGroups.ContainsKey(groupId))
						{
							myGroups[groupId].UpdateVehicles(units);
						}
						else
						{
							myGroups.Add(groupId, new VehiclesGroup(units));
							if (myGroups[groupId].IsGround)
							{
								if (myGroups[groupId].Count() > 50 && world.TickIndex < 1500)
								{
									if (!isTankSeparate || myGroups[groupId].Vehicles.First().Type != VehicleType.Tank)
									{
										ScaleUnitsByGroup(groupId, 0.1, myGroups[groupId].Center, 5, LastMoveTick(groupId));
										myGroups[groupId].DoNotMoveTill = LastMoveTick(groupId * 2) + 20;
									}
								}
								else
								{
									myGroups[groupId].IsBuilding = true;
								}
							}
							else
							{
								if (myGroups[groupId].Count() > 50 && world.TickIndex < 1500)
								{
									myGroups[groupId].DoNotMoveTill = LastMoveTick() + 100;
								}
								else
								{
									myGroups[groupId].IsBuilding = true;
								}
							}
						}
					}
					else if (myGroups.ContainsKey(groupId))
					{
						myGroups.Remove(groupId);
					}
				}
			}
			if (world.TickIndex == 0)
			{
				rnd = new Random((int)(game.RandomSeed % 10000000));
				for (var i = 0; i < 2; i++)
					for (var j = i + 1; j < 3; j++)
						if ((vehiclesByType[moveOrder[j]].Center.Y.More(vehiclesByType[moveOrder[i]].Center.Y)) ||
							(vehiclesByType[moveOrder[j]].Center.Y.Eq(vehiclesByType[moveOrder[i]].Center.Y) && vehiclesByType[moveOrder[j]].Center.X.More(vehiclesByType[moveOrder[i]].Center.X)))
						{
							var o = moveOrder[i];
							moveOrder[i] = moveOrder[j];
							moveOrder[j] = o;
						}
				tickHistory.Add("KillHelicopters", world.TickCount);
				tickHistory.Add("HideFighters", world.TickCount);
				tickHistory.Add("GoArmy", world.TickCount);
				tickHistory.Add("NuclearFighters", world.TickCount);
				tickHistory.Add("HisNuclear", world.TickCount);
				foreach (VehicleType vehicleType in Enum.GetValues(typeof(VehicleType)))
				{
					IsSpreadedByType.Add(vehicleType, false);
				}
				bestNuclearPoint = FindBestNuclearPoint(32);
			}
		}

		private bool ExecuteDelayedMove()
		{
			var delayedMove = moves.GetMove(world.TickIndex);
			if (delayedMove == null)
			{
				return false;
			}
			movesHistory.Add(Tuple.Create(world.TickIndex, delayedMove));
			if (delayedMove.Action == ActionType.ClearAndSelect)
			{
				var selected = myVehicles.Where(q => q.IsSelected);
				var required = myVehicles.Where(q =>
					(delayedMove.Group > 0 && delayedMove.Group.In(q.Groups)) ||
					((delayedMove.VehicleType == null || q.Type == delayedMove.VehicleType) && q.X.MoreEq(delayedMove.Left) && q.X.LessEq(delayedMove.Right) && q.Y.MoreEq(delayedMove.Top) && q.Y.LessEq(delayedMove.Bottom))
				);
				if (required.Count() == selected.Count() && required.Count() == required.Where(q => q.IsSelected).Count())
				{
					delayedMove = moves.GetMove(world.TickIndex);
					if (delayedMove == null)
					{
						return false;
					}
					movesHistory.Add(Tuple.Create(world.TickIndex, delayedMove));
				}
			}
			if (delayedMove.Action == ActionType.TacticalNuclearStrike && isFacilities)
			{
				var ok = false;
				var id = delayedMove.VehicleId;
				if (vehicleById.ContainsKey(id) && vehicleById[id].Groups.Length > 0 && myGroups.ContainsKey(vehicleById[id].Groups[0]))
				{
					var group = myGroups[vehicleById[id].Groups[0]];
					var points = GetVisiblePoints(group, 2);
					var nuclearP = FindBestNuclearPoint(points);
					if (nuclearP.damage > 0)
					{
						var p = nuclearP.p;
						var aimers = group.Vehicles.Where(q => p.Dist(q).Less(VisionFactor(q) * VisionRangeByType(q.Type) - 1));
						if (aimers.Count() > 0)
						{
							delayedMove.VehicleId = aimers.Last().Id;
							delayedMove.X = p.X;
							delayedMove.Y = p.Y;
							ok = true;
						}
					}
				}
				if (!ok)
				{
					delayedMove = moves.GetMove(world.TickIndex);
					if (delayedMove == null)
					{
						return false;
					}
					movesHistory.Add(Tuple.Create(world.TickIndex, delayedMove));
				}
			}
			move.Action = delayedMove.Action;
			move.Group = delayedMove.Group;
			move.Left = delayedMove.Left;
			move.Top = delayedMove.Top;
			move.Right = delayedMove.Right;
			move.Bottom = delayedMove.Bottom;
			move.X = delayedMove.X;
			move.Y = delayedMove.Y;
			move.Angle = delayedMove.Angle;
			move.Factor = delayedMove.Factor;
			move.MaxSpeed = delayedMove.MaxSpeed;
			move.MaxAngularSpeed = delayedMove.MaxAngularSpeed;
			move.VehicleType = delayedMove.VehicleType;
			move.FacilityId = delayedMove.FacilityId;
			move.VehicleId = delayedMove.VehicleId;
			return true;
		}

		private void QueueMoves(params Move[] move)
		{
			moves.Add(move);
		}

		private void QueueMoves(int tickToExecute, params Move[] move)
		{
			moves.Add(tickToExecute, move);
		}

		private Move QueueMove(Move move)
		{
			return move;
		}

		private Move SelectUnitsByType(VehicleType t)
		{
			return QueueMove(new Move
			{
				Action = ActionType.ClearAndSelect,
				Right = world.Width,
				Bottom = world.Height,
				VehicleType = t
			});
		}

		private Move SelectUnitsByTypeAdd(VehicleType t)
		{
			return QueueMove(new Move
			{
				Action = ActionType.AddToSelection,
				Right = world.Width,
				Bottom = world.Height,
				VehicleType = t
			});
		}

		private Move SelectUnitsByGroup(int g)
		{
			return QueueMove(new Move
			{
				Action = ActionType.ClearAndSelect,
				Group = g
			});
		}

		private Move SelectUnitsByGroupAdd(int g)
		{
			return QueueMove(new Move
			{
				Action = ActionType.AddToSelection,
				Group = g
			});
		}

		private void MoveUnitsByType(VehicleType t, Point p, double maxSpeed = 5, int tickToExecute = 0)
		{
			var m1 = SelectUnitsByType(t);
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Move,
				MaxSpeed = maxSpeed,
				X = p.X - vehiclesByType[t].Center.X,
				Y = p.Y - vehiclesByType[t].Center.Y
			});
			QueueMoves(tickToExecute, m1, m2);
		}

		private void MoveUnits(Point p1, Point p2, Point p, double maxSpeed = 5)
		{
			var m1 = QueueMove(new Move
			{
				Action = ActionType.ClearAndSelect,
				Left = p1.X,
				Right = p2.X,
				Top = p1.Y,
				Bottom = p2.Y
			});
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Move,
				MaxSpeed = maxSpeed,
				X = p.X,
				Y = p.Y
			});
			QueueMoves(m1, m2);
		}

		private void MoveUnitsByTypePartly(VehicleType t, Point p1, Point p2, Point p, double maxSpeed = 5, int tickToExecute = 0)
		{
			var m1 = QueueMove(new Move
			{
				Action = ActionType.ClearAndSelect,
				Left = p1.X,
				Right = p2.X,
				Top = p1.Y,
				Bottom = p2.Y,
				VehicleType = t
			});
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Move,
				MaxSpeed = maxSpeed,
				X = p.X,
				Y = p.Y
			});
			QueueMoves(tickToExecute, m1, m2);
		}

		private void MoveUnitsByGroup(int g, Point p, double maxSpeed = 5, int tickToExecute = 0)
		{
			var m1 = SelectUnitsByGroup(g);
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Move,
				MaxSpeed = maxSpeed,
				X = p.X,
				Y = p.Y
			});
			QueueMoves(tickToExecute, m1, m2);
			if (myGroups.ContainsKey(g))
			{
				myGroups[g].TickMoved = LastMoveTick();
			}
		}

		private void ScaleUnitsByType(VehicleType t, double factor, int tickToExecute = 0)
		{
			var m1 = SelectUnitsByType(t);
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Scale,
				Factor = factor,
				X = vehiclesByType[t].Center.X,
				Y = vehiclesByType[t].Center.Y
			});
			QueueMoves(tickToExecute, m1, m2);
		}

		private void ScaleUnitsByType(VehicleType t, double factor, Point p, int tickToExecute = 0)
		{
			var m1 = SelectUnitsByType(t);
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Scale,
				Factor = factor,
				X = p.X,
				Y = p.Y
			});
			QueueMoves(tickToExecute, m1, m2);
		}

		private void ScaleUnitsByGroup(int g, double factor, Point p, double maxSpeed = 5, int tickToExecute = 0)
		{
			var m1 = SelectUnitsByGroup(g);
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Scale,
				Factor = factor,
				MaxSpeed = maxSpeed,
				X = p.X,
				Y = p.Y
			});
			QueueMoves(tickToExecute, m1, m2);
			if (myGroups.ContainsKey(g))
			{
				myGroups[g].ScaledAtTick = LastMoveTick();
			}
		}

		private void RotateUnitsByType(VehicleType t, double angle, int tickToExecute = 0)
		{
			var m1 = SelectUnitsByType(t);
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Rotate,
				Angle = angle,
				X = vehiclesByType[t].Center.X,
				Y = vehiclesByType[t].Center.Y
			});
			QueueMoves(tickToExecute, m1, m2);
		}

		private void RotateUnitsByGroup(int g, double angle, Point p, double maxSpeed = 5, int tickToExecute = 0)
		{
			var m1 = SelectUnitsByGroup(g);
			var m2 = QueueMove(new Move
			{
				Action = ActionType.Rotate,
				Angle = angle,
				MaxSpeed = maxSpeed,
				X = p.X,
				Y = p.Y
			});
			QueueMoves(tickToExecute, m1, m2);
			if (myGroups.ContainsKey(g))
			{
				myGroups[g].RotatedAtTick = LastMoveTick();
			}
		}

		private void RotateUnitsByGroup(int g, double angle, double maxSpeed = 5, int tickToExecute = 0)
		{
			RotateUnitsByGroup(g, angle, myGroups[g].Center, maxSpeed, tickToExecute);
		}

		public void SetFactoryVehicleType(Facility facility, VehicleType t)
		{
			facilitiesAdd[facility.Id].ProductionChanged = LastMoveTick();
			if (facilitiesAdd[facility.Id].ProductionStarted == 0)
			{
				facilitiesAdd[facility.Id].ProductionStarted = LastMoveTick();
			}
			facilitiesAdd[facility.Id].IsGround = !t.In(VehicleType.Fighter, VehicleType.Helicopter);
			var m1 = new Move
			{
				Action = ActionType.SetupVehicleProduction,
				FacilityId = facility.Id,
				VehicleType = t
			};
			QueueMoves(m1);
		}

		private bool CanSpread(VehicleType t)
		{
			if (IsMovedByType[t] || vehiclesByType[t].Width.More(60) || vehiclesByType[t].Height.More(60) || IsSpreadedByType[t])
			{
				return false;
			}
			var tl = new Point();
			var br = new Point();
			if (spreadType == "vert")
			{
				tl.X = vehiclesByType[t].TopLeft.X - 3;
				tl.Y = 0;
				br.X = vehiclesByType[t].BottomRight.X + 3;
				br.Y = 250;
			}
			else
			{
				tl.X = 0;
				tl.Y = vehiclesByType[t].TopLeft.Y - 3;
				br.X = 250;
				br.Y = vehiclesByType[t].BottomRight.Y + 3;
			}
			if (isTankSeparate)
			{
				var maxX = Math.Max(vehiclesByType[VehicleType.Ifv].BottomRight.X, vehiclesByType[VehicleType.Arrv].BottomRight.X);
				var maxY = Math.Max(vehiclesByType[VehicleType.Ifv].BottomRight.Y, vehiclesByType[VehicleType.Arrv].BottomRight.Y);
				if (spreadType == tankGoDir || vehiclesByType[VehicleType.Tank].TopLeft.X.More(maxX) || vehiclesByType[VehicleType.Tank].TopLeft.Y.More(maxY))
				{
					return true;
				}
			}
			else if (vehicleById.Select(q => q.Value)
				.Where(q => q.Type.In(VehicleType.Arrv, VehicleType.Ifv, VehicleType.Tank))
				.Where(q => q.X.More(tl.X) && q.X.Less(br.X) && q.Y.More(tl.Y) && q.Y.Less(br.Y))
				.Count() == 100)
			{
				return true;
			}
			return false;
		}

		private void SpreadUnits(VehicleType t)
		{
			IsSpreadedByType[t] = true;
			var d = new Dictionary<VehicleType, int>();
			d.Add(VehicleType.Arrv, (isTankSeparate ? 30 : 6));
			d.Add(VehicleType.Ifv, (isTankSeparate ? 36 : 0));
			d.Add(VehicleType.Tank, 12);
			d.Add(VehicleType.Helicopter, 6);
			var units = vehiclesByType[t];
			var koef = isTankSeparate ? 12 : 18;
			if (spreadType == "vert")
			{
				var b = 1;
				var e = 10;
				while (b <= e)
				{
					if (units.TopLeft.Y.More(b * koef + d[t]))
					{
						MoveUnitsByTypePartly(
							t,
							new Point(units.TopLeft.X - 3, units.TopLeft.Y - 3 + (b - 1) * 6),
							new Point(units.BottomRight.X + 3, units.TopLeft.Y + 3 + (b - 1) * 6),
							new Point(0, b * koef - (units.TopLeft.Y + (b - 1) * 6) + d[t])
						);
						b++;
					}
					else if ((units.TopLeft.Y + (b - 1) * 6).Eq(b * koef + d[t]))
					{
						b++;
					}
					else if ((units.BottomRight.Y - (10 - e) * 6).Eq(e * koef + d[t]))
					{
						e--;
					}
					else
					{
						MoveUnitsByTypePartly(
							t,
							new Point(units.TopLeft.X - 3, units.TopLeft.Y - 3 + (e - 1) * 6),
							new Point(units.BottomRight.X + 3, units.TopLeft.Y + 3 + (e - 1) * 6),
							new Point(0, e * koef - (units.TopLeft.Y + (e - 1) * 6) + d[t])
						);
						e--;
					}
				}
			}
			else
			{
				var b = 1;
				var e = 10;
				while (b <= e)
				{
					if (units.TopLeft.X.More(b * koef + d[t]))
					{
						MoveUnitsByTypePartly(
							t,
							new Point(units.TopLeft.X - 3 + (b - 1) * 6, units.TopLeft.Y - 3),
							new Point(units.TopLeft.X + 3 + (b - 1) * 6, units.BottomRight.Y + 3),
							new Point(b * koef - (units.TopLeft.X + (b - 1) * 6) + d[t], 0)
						);
						b++;
					}
					else if ((units.TopLeft.X + (b - 1) * 6).Eq(b * koef + d[t]))
					{
						b++;
					}
					else if ((units.BottomRight.X - (10 - e) * 6).Eq(e * koef + d[t]))
					{
						e--;
					}
					else
					{
						MoveUnitsByTypePartly(
							t,
							new Point(units.TopLeft.X - 3 + (e - 1) * 6, units.TopLeft.Y - 3),
							new Point(units.TopLeft.X + 3 + (e - 1) * 6, units.BottomRight.Y + 3),
							new Point(e * koef - (units.TopLeft.X + (e - 1) * 6) + d[t], 0)
						);
						e--;
					}
				}
			}
		}

		private Vehicle FindClosestVehicleByType(VehicleType t, Point p)
		{
			Vehicle result = null;
			var minD = 2000.0D;
			foreach (var item in opponentVehiclesByType[t].Vehicles)
			{
				var d = item.GetDistanceTo(p.X, p.Y);
				if (d < minD)
				{
					minD = d;
					result = item;
				}
			}
			return result;
		}

		public int GetActionCountPer60()
		{
			if (!isFacilities)
			{
				return game.BaseActionCount;
			}
			else
			{
				return game.BaseActionCount + myFacilities.Where(q => q.Type == FacilityType.ControlCenter).Count() * game.AdditionalActionCountPerControlCenter;
			}
		}

		public int LastMoveTick(int reserve = 1)
		{
			var ticks = new List<int>();
			for (int i = 1; i < 60; i++)
			{
				ticks.Add(0);
			}
			foreach (var i in movesHistory.Select(q => q.Item1).Where(q => q > world.TickIndex - 60))
			{
				ticks[world.TickIndex - i - 1] = 1;
			}
			var movesCount = moves.FutureMovesCount + reserve;
			while (movesCount > 0)
			{
				if (ticks.Skip(ticks.Count - 59).Sum() < GetActionCountPer60())
				{
					ticks.Add(1);
					movesCount--;
				}
				else
				{
					ticks.Add(0);
				}
			}
			return world.TickIndex + ticks.Count - 60 - 1;
		}

		private double VisionFactor(VehicleType t, Point p)
		{
			if (p.X.LessEq(0) || p.X.MoreEq(game.WorldWidth) || p.Y.LessEq(0) || p.Y.MoreEq(game.WorldHeight))
			{
				return 0;
			}
			if (t.In(VehicleType.Fighter, VehicleType.Helicopter))
			{
				var w = weather[(int)Math.Floor(p.X / 32)][(int)Math.Floor(p.Y / 32)];
				if (w == WeatherType.Clear)
					return game.ClearWeatherVisionFactor;
				else if (w == WeatherType.Cloud)
					return game.CloudWeatherVisionFactor;
				else
					return game.RainWeatherVisionFactor;
			}
			else
			{
				var w = terrain[(int)Math.Floor(p.X / 32)][(int)Math.Floor(p.Y / 32)];
				if (w == TerrainType.Plain)
					return game.PlainTerrainVisionFactor;
				else if (w == TerrainType.Forest)
					return game.ForestTerrainVisionFactor;
				else
					return game.SwampTerrainVisionFactor;
			}
		}

		private double VisionFactor(Vehicle v)
		{
			return VisionFactor(v.Type, new Point(v));
		}

		private double VisionRangeByType(VehicleType t)
		{
			if (t == VehicleType.Arrv)
				return game.ArrvVisionRange;
			else if (t == VehicleType.Fighter)
				return game.FighterVisionRange;
			else if (t == VehicleType.Helicopter)
				return game.HelicopterVisionRange;
			else if (t == VehicleType.Ifv)
				return game.IfvVisionRange;
			else
				return game.TankVisionRange;
		}

		private double SpeedFactor(VehicleType t, Point p)
		{
			if (p.X.LessEq(0) || p.X.MoreEq(game.WorldWidth) || p.Y.LessEq(0) || p.Y.MoreEq(game.WorldHeight))
			{
				return 0;
			}
			if (t.In(VehicleType.Fighter, VehicleType.Helicopter))
			{
				var w = weather[(int)Math.Floor(p.X / 32)][(int)Math.Floor(p.Y / 32)];
				if (w == WeatherType.Clear)
					return game.ClearWeatherSpeedFactor;
				else if (w == WeatherType.Cloud)
					return game.CloudWeatherSpeedFactor;
				else
					return game.RainWeatherSpeedFactor;
			}
			else
			{
				var w = terrain[(int)Math.Floor(p.X / 32)][(int)Math.Floor(p.Y / 32)];
				if (w == TerrainType.Plain)
					return game.PlainTerrainSpeedFactor;
				else if (w == TerrainType.Forest)
					return game.ForestTerrainSpeedFactor;
				else
					return game.SwampTerrainSpeedFactor;
			}
		}

		private double SpeedFactor(Vehicle v)
		{
			return SpeedFactor(v.Type, new Point(v));
		}

		private double SpeedFactor(Vehicle v, Point p)
		{
			return SpeedFactor(v.Type, p);
		}

		private void GroundStep1()
		{
			var arrvs = vehiclesByType[VehicleType.Arrv];
			var ifvs = vehiclesByType[VehicleType.Ifv];
			var tanks = vehiclesByType[VehicleType.Tank];
			if (isFacilities)
			{
				var canGoVert = (!tanks.Center.X.Eq(arrvs.Center.X) || tanks.Center.Y.More(arrvs.Center.Y)) && (!tanks.Center.X.Eq(ifvs.Center.X) || tanks.Center.Y.More(ifvs.Center.Y));
				var canGoHor = (!tanks.Center.Y.Eq(arrvs.Center.Y) || tanks.Center.X.More(arrvs.Center.X)) && (!tanks.Center.Y.Eq(ifvs.Center.Y) || tanks.Center.X.More(ifvs.Center.X));
				if (canGoVert || canGoHor)
				{
					Facility closestX = null;
					Facility closestY = null;
					if (canGoVert)
					{
						closestX = facilities.Where(q => q.Top.More(250)).OrderBy(q => tanks.Center.Dist(q)).First();
						tankGoDir = "vert";
					}
					if (canGoHor)
					{
						closestY = facilities.Where(q => q.Left.More(250)).OrderBy(q => tanks.Center.Dist(q)).First();
						if (closestX == null)
						{
							tankGoDir = "hor";
						}
						else
						{
							if (tanks.Center.Dist(closestY).Eq(tanks.Center.Dist(closestX)))
							{
								tankGoDir = "both";
							}
							else if (tanks.Center.Dist(closestY).Less(tanks.Center.Dist(closestX)))
							{
								tankGoDir = "hor";
							}
						}
					}
					Point separateTankFirstPoint;
					if (tankGoDir == "both")
					{
						if (!arrvs.Center.Y.Eq(ifvs.Center.Y))
						{
							tankGoDir = "hor";
							spreadType = "hor";
						}
						else if (!arrvs.Center.X.Eq(ifvs.Center.X))
						{
							tankGoDir = "vert";
							spreadType = "vert";
						}
					}
					else if (tankGoDir == "hor")
					{
						separateTankFirstPoint = new Point(300, tanks.Center.Y);
						if (!arrvs.Center.Y.Eq(ifvs.Center.Y))
						{
							spreadType = "hor";
						}
						else if (!arrvs.Center.X.Eq(ifvs.Center.X))
						{
							spreadType = "vert";
						}
					}
					else
					{
						separateTankFirstPoint = new Point(tanks.Center.X, 300);
						if (!arrvs.Center.X.Eq(ifvs.Center.X))
						{
							spreadType = "vert";
						}
						else if (!arrvs.Center.Y.Eq(ifvs.Center.Y))
						{
							spreadType = "hor";
						}
					}
					if (tankGoDir != spreadType && spreadType != "")
					{
						if (spreadType == "hor" && canGoHor)
						{
							tankGoDir = "hor";
						}
						else if (spreadType == "vert" && canGoVert)
						{
							tankGoDir = "vert";
						}
					}
					if (tankGoDir == spreadType)
					{
						var m1 = SelectUnitsByType(VehicleType.Tank);
						var m2 = QueueMove(new Move
						{
							Action = ActionType.Assign,
							Group = NextGroupId
						});
						QueueMoves(m1, m2);
						isTankSeparate = true;
						return;
					}
				}
			}
			if (!arrvs.Center.X.Eq(ifvs.Center.X) && !arrvs.Center.X.Eq(tanks.Center.X) && !ifvs.Center.X.Eq(tanks.Center.X))
			{
				spreadType = "vert";
			}
			else if (!arrvs.Center.Y.Eq(ifvs.Center.Y) && !arrvs.Center.Y.Eq(tanks.Center.Y) && !ifvs.Center.Y.Eq(tanks.Center.Y))
			{
				spreadType = "hor";
			}
			else
			{
				if (isFacilities)
				{
					for (var i = 0; i < 3; i++)
					{
						var m1 = SelectUnitsByType(i == 0 ? VehicleType.Arrv : i == 1 ? VehicleType.Tank : VehicleType.Ifv);
						var m2 = QueueMove(new Move
						{
							Action = ActionType.Assign,
							Group = NextGroupId
						});
						QueueMoves(m1, m2);
					}
					isAllSeparate = true;
					return;
				}
				var countTL = myVehiclesLand.Where(q => q.X.Less(100) && q.Y.Less(100)).Count();
				var countTR = myVehiclesLand.Where(q => q.X.More(138) && q.Y.Less(100)).Count();
				var countBL = myVehiclesLand.Where(q => q.X.Less(100) && q.Y.More(138)).Count();
				var countBR = myVehiclesLand.Where(q => q.X.More(138) && q.Y.More(138)).Count();
				if (countTL > 130)
				{
					MoveUnitsByType(moveOrder[1], new Point(vehiclesByType[moveOrder[1]].Center.X + 74, vehiclesByType[moveOrder[1]].Center.Y));
					MoveUnitsByType(moveOrder[2], new Point(vehiclesByType[moveOrder[2]].Center.X + 74, vehiclesByType[moveOrder[2]].Center.Y));
					spreadType = "vert";
				}
				else if (countTR > 130)
				{
					MoveUnitsByType(moveOrder[0], new Point(vehiclesByType[moveOrder[0]].Center.X - 74, vehiclesByType[moveOrder[0]].Center.Y));
					MoveUnitsByType(moveOrder[2], new Point(vehiclesByType[moveOrder[2]].Center.X - 74, vehiclesByType[moveOrder[2]].Center.Y));
					spreadType = "vert";
				}
				else if (countBL > 130)
				{
					MoveUnitsByType(moveOrder[0], new Point(vehiclesByType[moveOrder[0]].Center.X + 74, vehiclesByType[moveOrder[0]].Center.Y));
					MoveUnitsByType(moveOrder[2], new Point(vehiclesByType[moveOrder[2]].Center.X + 74, vehiclesByType[moveOrder[2]].Center.Y));
					spreadType = "vert";
				}
				else if (countBR > 130)
				{
					MoveUnitsByType(moveOrder[0], new Point(vehiclesByType[moveOrder[0]].Center.X - 74, vehiclesByType[moveOrder[0]].Center.Y));
					MoveUnitsByType(moveOrder[1], new Point(vehiclesByType[moveOrder[1]].Center.X - 74, vehiclesByType[moveOrder[1]].Center.Y));
					spreadType = "vert";
				}
				else
				{
					if (ifvs.Center.X.More(74) && !arrvs.Center.X.Eq(ifvs.Center.X - 74) && !arrvs.Center.X.Eq(tanks.Center.X) && !tanks.Center.X.Eq(ifvs.Center.X - 74))
					{
						MoveUnitsByType(VehicleType.Ifv, new Point(ifvs.Center.X - 74, ifvs.Center.Y));
						spreadType = "vert";
					}
					else if (ifvs.Center.X.Less(150) && !arrvs.Center.X.Eq(ifvs.Center.X + 74) && !arrvs.Center.X.Eq(tanks.Center.X) && !tanks.Center.X.Eq(ifvs.Center.X + 74))
					{
						MoveUnitsByType(VehicleType.Ifv, new Point(ifvs.Center.X + 74, ifvs.Center.Y));
						spreadType = "vert";
					}
					else if (ifvs.Center.Y.More(74) && !arrvs.Center.Y.Eq(ifvs.Center.Y - 74) && !arrvs.Center.Y.Eq(tanks.Center.Y) && !tanks.Center.Y.Eq(ifvs.Center.Y - 74))
					{
						MoveUnitsByType(VehicleType.Ifv, new Point(ifvs.Center.X, ifvs.Center.Y - 74));
						spreadType = "hor";
					}
					else if (ifvs.Center.Y.Less(150) && !arrvs.Center.Y.Eq(ifvs.Center.Y + 74) && !arrvs.Center.Y.Eq(tanks.Center.Y) && !tanks.Center.Y.Eq(ifvs.Center.Y + 74))
					{
						MoveUnitsByType(VehicleType.Ifv, new Point(ifvs.Center.X, ifvs.Center.Y + 74));
						spreadType = "hor";
					}
					else if (arrvs.Center.X.More(74) && !tanks.Center.X.Eq(arrvs.Center.X - 74) && !ifvs.Center.X.Eq(tanks.Center.X) && !ifvs.Center.X.Eq(arrvs.Center.X - 74))
					{
						MoveUnitsByType(VehicleType.Arrv, new Point(arrvs.Center.X - 74, arrvs.Center.Y));
						spreadType = "vert";
					}
					else if (arrvs.Center.X.Less(150) && !tanks.Center.X.Eq(arrvs.Center.X + 74) && !ifvs.Center.X.Eq(tanks.Center.X) && !ifvs.Center.X.Eq(arrvs.Center.X + 74))
					{
						MoveUnitsByType(VehicleType.Arrv, new Point(arrvs.Center.X + 74, arrvs.Center.Y));
						spreadType = "vert";
					}
					else if (arrvs.Center.Y.More(74) && !tanks.Center.Y.Eq(arrvs.Center.Y - 74) && !ifvs.Center.Y.Eq(tanks.Center.Y) && !ifvs.Center.Y.Eq(arrvs.Center.Y - 74))
					{
						MoveUnitsByType(VehicleType.Arrv, new Point(arrvs.Center.X, arrvs.Center.Y - 74));
						spreadType = "hor";
					}
					else if (arrvs.Center.Y.Less(150) && !tanks.Center.Y.Eq(arrvs.Center.Y + 74) && !ifvs.Center.Y.Eq(tanks.Center.Y) && !ifvs.Center.Y.Eq(arrvs.Center.Y + 74))
					{
						MoveUnitsByType(VehicleType.Arrv, new Point(arrvs.Center.X, arrvs.Center.Y + 74));
						spreadType = "hor";
					}
					else if (tanks.Center.X.More(74) && !arrvs.Center.X.Eq(tanks.Center.X - 74) && !arrvs.Center.X.Eq(ifvs.Center.X) && !ifvs.Center.X.Eq(tanks.Center.X - 74))
					{
						MoveUnitsByType(VehicleType.Tank, new Point(tanks.Center.X - 74, tanks.Center.Y));
						spreadType = "vert";
					}
					else if (tanks.Center.X.Less(150) && !arrvs.Center.X.Eq(tanks.Center.X + 74) && !arrvs.Center.X.Eq(ifvs.Center.X) && !ifvs.Center.X.Eq(tanks.Center.X + 74))
					{
						MoveUnitsByType(VehicleType.Tank, new Point(tanks.Center.X + 74, tanks.Center.Y));
						spreadType = "vert";
					}
					else if (tanks.Center.Y.More(74) && !arrvs.Center.Y.Eq(tanks.Center.Y - 74) && !arrvs.Center.Y.Eq(ifvs.Center.Y) && !ifvs.Center.Y.Eq(tanks.Center.Y - 74))
					{
						MoveUnitsByType(VehicleType.Tank, new Point(tanks.Center.X, tanks.Center.Y - 74));
						spreadType = "hor";
					}
					else if (tanks.Center.Y.Less(150) && !arrvs.Center.Y.Eq(tanks.Center.Y + 74) && !arrvs.Center.Y.Eq(ifvs.Center.Y) && !ifvs.Center.Y.Eq(tanks.Center.Y + 74))
					{
						MoveUnitsByType(VehicleType.Tank, new Point(tanks.Center.X, tanks.Center.Y + 74));
						spreadType = "hor";
					}
				}
			}
		}

		private void MakeFormation()
		{
			var fighters = vehiclesByType[VehicleType.Fighter];
			var helicopters = vehiclesByType[VehicleType.Helicopter];
			var arrvs = vehiclesByType[VehicleType.Arrv];
			var ifvs = vehiclesByType[VehicleType.Ifv];
			var tanks = vehiclesByType[VehicleType.Tank];

			if (world.TickIndex == 0)
			{
				if (fighters.Center.X > helicopters.Center.X || fighters.Center.Y > helicopters.Center.Y)
				{
					fightersPoint = fighters.Center * 2 - new Point(300, 300);
					helicoptersPoint = helicopters.Center * 2 - new Point(180, 294);
					ScaleUnitsByType(VehicleType.Fighter, 2, fightersPoint);
				}
				else
				{
					fightersPoint = fighters.Center * 2 - new Point(180, 294);
					helicoptersPoint = helicopters.Center * 2 - new Point(300, 300);
					ScaleUnitsByType(VehicleType.Helicopter, 2, helicoptersPoint);
				}
				GroundStep1();
				stepAir++;
				stepGround++;
			}
			if (stepAir == 2 && world.TickIndex > 20 && (fighters.TopLeft > helicopters.BottomRight || helicopters.Center.X > 250 && helicopters.Center.Y > 250 || helicopters.TopLeft > fighters.BottomRight + new Point(60, 60)))
			{
				if (fighters.TopLeft > helicopters.BottomRight)
				{
					ScaleUnitsByType(VehicleType.Helicopter, 2, helicoptersPoint);
				}
				else
				{
					ScaleUnitsByType(VehicleType.Fighter, 2, fightersPoint);
				}
				moves.BringToFrontLast();
				stepAir++;
			}
			if (stepAir == 3 && fighters.Center.Y.More(290) && helicopters.Center.Y.More(290) && !IsMovedByType[VehicleType.Fighter] && !IsMovedByType[VehicleType.Helicopter])
			{
				MoveUnitsByTypePartly(VehicleType.Fighter, new Point(0, 0), new Point(1000, 1000), new Point(helicopters.Center.X - fighters.Center.X + 6, 0), 5);
				moves.BringToFrontLast();
				stepAir++;
			}
			if (stepAir == 4 && fighters.Center.CloseTo(helicopters.Center, 10) && !IsMovedByType[VehicleType.Fighter] && !IsMovedByType[VehicleType.Helicopter])
			{
				var m1 = SelectUnitsByType(VehicleType.Fighter);
				var m2 = SelectUnitsByTypeAdd(VehicleType.Helicopter);
				var m3 = QueueMove(new Move
				{
					Action = ActionType.Assign,
					Group = NextGroupId
				});
				QueueMoves(m1, m2, m3);
				ScaleUnitsByGroup(lastGroupId, 0.5, (fighters.Center + helicopters.Center) / 2, 0.54);
				nuclearGroupId = lastGroupId;
				moves.BringToFrontLast(2);
				tickHistory["KillHelicopters"] = LastMoveTick() + 100;
				stepAir++;
			}
			if (isAllSeparate && isFacilities)
			{
				return;
			}
			if (stepGround == 2 && world.TickIndex > 10 && world.TickIndex % 5 == 1)
			{
				if (!isTankSeparate && CanSpread(VehicleType.Tank))
				{
					SpreadUnits(VehicleType.Tank);
				}
				if (CanSpread(VehicleType.Arrv))
				{
					SpreadUnits(VehicleType.Arrv);
				}
				if (CanSpread(VehicleType.Ifv))
				{
					SpreadUnits(VehicleType.Ifv);
				}
				if ((isTankSeparate || IsSpreadedByType[VehicleType.Tank]) && IsSpreadedByType[VehicleType.Arrv] && IsSpreadedByType[VehicleType.Ifv])
				{
					stepGround++;
				}
			}
			if (stepGround == 3 && world.TickIndex > 300 && !IsMovedByType[VehicleType.Arrv] && !IsMovedByType[VehicleType.Ifv] && (isTankSeparate && !vehiclesByType[VehicleType.Tank].TopLeft.Between(MathMiXa.ZeroPoint, new Point(160, 160)) || !isTankSeparate && !IsMovedByType[VehicleType.Tank]))
			{
				if (isTankSeparate)
				{
					if (spreadType == "vert")
					{
						MoveUnitsByType(VehicleType.Ifv, new Point((ifvs.Center.X + arrvs.Center.X) / 2 + 3, ifvs.Center.Y));
						MoveUnitsByType(VehicleType.Arrv, new Point((ifvs.Center.X + arrvs.Center.X) / 2, arrvs.Center.Y));
					}
					if (spreadType == "hor")
					{
						MoveUnitsByType(VehicleType.Ifv, new Point(ifvs.Center.X, (ifvs.Center.Y + arrvs.Center.Y) / 2 + 3));
						MoveUnitsByType(VehicleType.Arrv, new Point(arrvs.Center.X, (ifvs.Center.Y + arrvs.Center.Y) / 2));
					}
				}
				else
				{
					foreach (VehicleType vehicleType in Enum.GetValues(typeof(VehicleType)))
					{
						if (vehicleType != VehicleType.Fighter && vehicleType != VehicleType.Helicopter)
						{
							if (spreadType == "vert" && !vehiclesByType[vehicleType].Center.X.Eq(119))
							{
								MoveUnitsByType(vehicleType, new Point(119, vehiclesByType[vehicleType].Center.Y));
							}
							if (spreadType == "hor" && !vehiclesByType[vehicleType].Center.Y.Eq(119))
							{
								MoveUnitsByType(vehicleType, new Point(vehiclesByType[vehicleType].Center.X, 119));
							}
						}
					}
				}
				stepGround++;
			}
			if (stepGround == 4 && !IsMovedByType[VehicleType.Arrv] && !IsMovedByType[VehicleType.Ifv] && (isTankSeparate || !IsMovedByType[VehicleType.Tank]) &&
				vehiclesByType[VehicleType.Arrv].Center.Dist(vehiclesByType[VehicleType.Ifv].Center).Less(10) && (isTankSeparate || vehiclesByType[VehicleType.Arrv].Center.Dist(vehiclesByType[VehicleType.Tank].Center).Less(10)) &&
				(!isFacilities || myVehicles.Where(q => q.X.Between(Math.Min(ifvs.TopLeft.X, arrvs.TopLeft.X) - 3, Math.Max(ifvs.BottomRight.X, arrvs.BottomRight.X) + 3) && q.Y.Between(Math.Min(ifvs.TopLeft.Y, arrvs.TopLeft.Y) - 3, Math.Max(ifvs.BottomRight.Y, arrvs.BottomRight.Y) + 3)).Count() == 200))
				//(vehiclesByType[VehicleType.Arrv].Center.X.Eq(vehiclesByType[VehicleType.Ifv].Center.X) && (isTankSeparate || vehiclesByType[VehicleType.Arrv].Center.X.Eq(vehiclesByType[VehicleType.Tank].Center.X)) ||
				//vehiclesByType[VehicleType.Arrv].Center.Y.Eq(vehiclesByType[VehicleType.Ifv].Center.Y) && (isTankSeparate || vehiclesByType[VehicleType.Arrv].Center.Y.Eq(vehiclesByType[VehicleType.Tank].Center.Y))))
			{
				double angle;
				Move m1, m2;
				if (spreadType == "vert")
				{
					angle = Math.PI / 4;
					if (isFacilities)
					{
						if (facilities.Count() <= 12 || isTankSeparate)
						{
							for (var i = isTankSeparate ? 1 : 2; i >= 0; i--)
							{
								m1 = QueueMove(new Move
								{
									Action = ActionType.ClearAndSelect,
									Left = Math.Min(ifvs.TopLeft.X, arrvs.TopLeft.X) - 3,
									Right = Math.Max(ifvs.BottomRight.X, arrvs.BottomRight.X) + 3,
									Top = -3 + i * (36 + 36) + (isTankSeparate ? 30 : 0),
									Bottom = -3 + (36 + 36) + i * (36 + 36) + (isTankSeparate ? 30 : 0)
								});
								m2 = QueueMove(new Move
								{
									Action = ActionType.Assign,
									Group = NextGroupId
								});
								QueueMoves(m1, m2);
							}
						}
						else
						{
							var bottom = 15 + 42 * 4 + 12;
							for (var i = 3; i >= 0; i--)
							{
								m1 = QueueMove(new Move
								{
									Action = ActionType.ClearAndSelect,
									Left = 0,
									Right = 230,
									Top = bottom - (42 + 6 * (i % 2)),
									Bottom = bottom
								});
								m2 = QueueMove(new Move
								{
									Action = ActionType.Assign,
									Group = NextGroupId
								});
								QueueMoves(m1, m2);
								bottom = bottom - (42 + 6 * (i % 2));
							}
						}
					}
					else
					{
						m1 = QueueMove(new Move
						{
							Action = ActionType.ClearAndSelect,
							Left = 0,
							Right = 200,
							Top = 0,
							Bottom = 200
						});
						m2 = QueueMove(new Move
						{
							Action = ActionType.Assign,
							Group = Group.ARMY
						});
						QueueMoves(m1, m2);
					}
				}
				else
				{
					angle = -Math.PI / 4;
					if (isFacilities)
					{
						if (facilities.Count() <= 12 || isTankSeparate)
						{
							for (var i = isTankSeparate ? 1 : 2; i >= 0; i--)
							{
								m1 = QueueMove(new Move
								{
									Action = ActionType.ClearAndSelect,
									Left = -3 + i * (36 + 36) + (isTankSeparate ? 30 : 0),
									Right = -3 + (36 + 36) + i * (36 + 36) + (isTankSeparate ? 30 : 0),
									Top = Math.Min(ifvs.TopLeft.Y, arrvs.TopLeft.Y) - 3,
									Bottom = Math.Max(ifvs.BottomRight.Y, arrvs.BottomRight.Y) + 3
								});
								m2 = QueueMove(new Move
								{
									Action = ActionType.Assign,
									Group = NextGroupId
								});
								QueueMoves(m1, m2);
							}
						}
						else
						{
							var right = 15 + 42 * 4 + 12;
							for (var i = 3; i >= 0; i--)
							{
								m1 = QueueMove(new Move
								{
									Action = ActionType.ClearAndSelect,
									Left = right - (42 + 6 * (i % 2)),
									Right = right,
									Top = 0,
									Bottom = 230
								});
								m2 = QueueMove(new Move
								{
									Action = ActionType.Assign,
									Group = NextGroupId
								});
								QueueMoves(m1, m2);
								right = right - (42 + 6 * (i % 2));
							}
						}
					}
					else
					{
						m1 = QueueMove(new Move
						{
							Action = ActionType.ClearAndSelect,
							Left = 0,
							Right = 230,
							Top = 0,
							Bottom = 230
						});
						m2 = QueueMove(new Move
						{
							Action = ActionType.Assign,
							Group = Group.ARMY
						});
						QueueMoves(m1, m2);
					}
				}
				if (!isFacilities)
				{
					var p = (vehiclesByType[VehicleType.Arrv].Center + vehiclesByType[VehicleType.Ifv].Center + vehiclesByType[VehicleType.Tank].Center) / 3;
					var shiftP = new Point(10, 10);
					p += shiftP;
					MoveUnitsByGroup(Group.ARMY, shiftP, LastMoveTick() + 50);
					ScaleUnitsByGroup(Group.ARMY, 1.16, p, 5, LastMoveTick() + 100);
					RotateUnitsByGroup(Group.ARMY, angle, p, 0.3, LastMoveTick() + 200);
				}
				stepGround++;
			}
		}

		private List<Point> GetVisiblePoints(List<Vehicle> units, int precision = 16)
		{
			var result = new List<Point>();
			var fromI = Math.Max(0, units.Select(q => q.X).Min() - game.FighterVisionRange);
			var toI = Math.Min(game.WorldWidth, units.Select(q => q.X).Max() + game.FighterVisionRange);
			var fromJ = Math.Max(0, units.Select(q => q.Y).Min() - game.FighterVisionRange);
			var toJ = Math.Min(game.WorldHeight, units.Select(q => q.Y).Max() + game.FighterVisionRange);
			for (var i = fromI; i <= toI; i += precision)
			{
				for (var j = fromJ; j <= toJ; j += precision)
				{
					var p = new Point(i, j);
					foreach (var vehicle in units)
					{
						var vr = VisionRangeByType(vehicle.Type);
						var vf = VisionFactor(vehicle);
						if (vehicle.GetDistanceTo(i, j).Less(vr * vf - 2))
						{
							result.Add(p);
							break;
						}
					}
				}
			}
			return result;
		}

		private List<Point> GetVisiblePoints(int precision = 16)
		{
			return GetVisiblePoints(myVehicles, precision);
		}

		private List<Point> GetVisiblePoints(VehiclesGroup g, int precision = 16)
		{
			return GetVisiblePoints(g.Vehicles, precision);
		}

		private List<Point> GetVisiblePointsByType(VehicleType t, int precision = 16)
		{
			return GetVisiblePoints(vehiclesByType[t], precision);
		}

		private double GetDamage(Vehicle v, Point p)
		{
			var d = p.Dist(v);
			if (prevVehicleById.ContainsKey(v.Id))
			{
				d = p.Dist(new Point(v) * 2 - prevVehicleById[v.Id]);
			}
			var killDamage = 100;
			if (v.PlayerId != me.Id)
			{
				d += 20 * v.MaxSpeed;
			}
			var t = 100 - 2 * d;
			if (t.LessEq(0))
			{
				return 0;
			}
			else if (t > v.Durability)
			{
				return killDamage;
			}
			if (v.PlayerId != me.Id)
			{
				if (v.Type == VehicleType.Fighter)
					t += -5;
				if (v.Type == VehicleType.Helicopter)
					t += 0;
				if (v.Type == VehicleType.Arrv)
					t -= 70;
				if (v.Type == VehicleType.Ifv)
					t += 15;
				if (v.Type == VehicleType.Ifv && isFacilities)
					t -= 8;
				if (v.Type == VehicleType.Tank)
					t += 8;
			}
			if (!isFacilities && v.PlayerId != me.Id && myArmy.Count > 0 && v.GetDistanceTo(armyCenter.X, armyCenter.Y) > 10 && v.GetDistanceTo(armyCenter.X, armyCenter.Y) < 150)
			{
				if (v.Type == VehicleType.Fighter)
					t += 5;
				if (v.Type == VehicleType.Helicopter)
					t += 5;
				if (v.Type == VehicleType.Arrv)
					t += 0;
				if (v.Type == VehicleType.Ifv)
					t += 10;
				if (v.Type == VehicleType.Tank)
					t += 10;
			}
			t = Math.Max(0, t);
			return t > killDamage ? killDamage - 5 : Math.Min(99, t);
		}

		private double GetDamage(List<Vehicle> vehicles, Point p)
		{
			return vehicles.Where(q => q.GetDistanceTo(p.X, p.Y).Less(game.TacticalNuclearStrikeRadius)).Select(q => { return GetDamage(q, p); }).Sum();
		}

		private double GetDamage(Point p)
		{
			var his = GetDamage(hisVehicles, p);
			var my = GetDamage(myVehicles, p);
			return his - my;
		}

		private NuclearPoint FindBestNuclearPoint(List<Point> points)
		{
			Point result = new Point();
			double maxDamage = 0;
			double damage = 0;
			foreach (var p in points)
			{
				damage = GetDamage(p);
				if (damage > maxDamage)
				{
					maxDamage = damage;
					result = p;
				}
			}
			if (maxDamage.LessEq(0))
			{
				result = new Point(-1, -1);
			}
			return new NuclearPoint(result, maxDamage);
		}

		private NuclearPoint FindBestNuclearPoint(int precision = 16)
		{
			return FindBestNuclearPoint(new Point(0, 0), new Point(game.WorldWidth, game.WorldHeight), precision);
		}

		private NuclearPoint FindBestNuclearPoint(Point topleft, Point bottomright, int precision = 16)
		{
			List<Point> points = new List<Point>();
			var fromI = (int)Math.Floor(topleft.X / precision);
			var toI = (int)Math.Floor(bottomright.X / precision);
			var fromJ = (int)Math.Floor(topleft.Y / precision);
			var toJ = (int)Math.Floor(bottomright.Y / precision);
			for (var i = fromI; i < toI; i++)
			{
				for (var j = fromJ; j < toJ; j++)
				{
					points.Add(new Point(i * precision, j * precision));
				}
			}
			return FindBestNuclearPoint(points);
		}

		private Vehicle GetClosestUnitByType(VehicleType t, Point p)
		{
			return vehiclesByType[t].Vehicles.OrderBy(q => q.GetSquaredDistanceTo(p.X, p.Y)).First();
		}

		private Vehicle GetClosestOpponent(Point p, string type = "all")
		{
			IEnumerable<Vehicle> units;
			if (type == "aerial")
			{
				units = hisVehicles.Where(q => q.IsAerial);
			} else if (type == "land")
			{
				units = hisVehiclesLand;
			}
			else
			{
				units = hisVehicles;
			}
			return units.OrderBy(q => q.GetSquaredDistanceTo(p.X, p.Y)).FirstOrDefault();
		}

		private Vehicle GetClosestAlly(Point p)
		{
			return myVehicles.OrderBy(q => q.GetSquaredDistanceTo(p.X, p.Y)).First();
		}

		private Vehicle GetClosestAlly(Vehicle v)
		{
			return GetClosestAlly(new Point(v));
		}

		private Vehicle GetClosestUnit(Point p, IEnumerable<Vehicle> units)
		{
			return units.OrderBy(q => q.GetSquaredDistanceTo(p.X, p.Y)).First();
		}

		private Vehicle GetClosestUnit(Vehicle v, IEnumerable<Vehicle> units)
		{
			return GetClosestUnit(new Point(v), units);
		}

		private Vehicle GetFarthestUnit(Point p, IEnumerable<Vehicle> units)
		{
			return units.OrderBy(q => q.GetSquaredDistanceTo(p.X, p.Y)).Last();
		}

		private Facility GetFacility(Point p)
		{
			foreach (var facility in facilities)
			{
				if (p.X.More(facility.Left) && p.X.Less(facility.Left + game.FacilityWidth) && p.Y.More(facility.Top) && p.Y.Less(facility.Top + game.FacilityHeight))
				{
					return facility;
				}
			}
			return null;
		}

		private void MoveUnitsByGroupToCheckPoint(int groupId)
		{
			var group = myGroups[groupId];
			if (group.CheckPoint.Dist(group.Center).More(group.IsGround ? 3 : 6))
			{
				double maxSpeed = 2;
				double realMaxSpeed = 0;
				var parts = 6;
				var vector = (group.CheckPoint - group.Center) / parts;
				foreach (var vehicle in group.Vehicles)
				{
					if (vehicle.MaxSpeed.More(realMaxSpeed))
					{
						realMaxSpeed = vehicle.MaxSpeed;
					}
					var dest = new Point(vehicle);
					double speedFactor = 2;
					for (var i = 0; i <= parts; i++)
					{
						dest += vector;
						speedFactor = Math.Min(speedFactor, SpeedFactor(vehicle, dest));
					}
					maxSpeed = Math.Min(maxSpeed, speedFactor * vehicle.MaxSpeed);
					if (maxSpeed.Eq(0.18))
					{
						break;
					}
				}
				var types = group.Vehicles.Select(q => q.Type).Distinct().ToList();
				if (types.Count == 1 || types.Count == 2 && types[0].In(VehicleType.Arrv, VehicleType.Ifv) && types[1].In(VehicleType.Arrv, VehicleType.Ifv))
				{
					maxSpeed = (maxSpeed + realMaxSpeed) / 2;
				}
				else if (!group.IsGround)
				{
					maxSpeed = (maxSpeed + game.HelicopterSpeed) / 2; // Math.Min(maxSpeed * 1.1, game.HelicopterSpeed);
				}
				MoveUnitsByGroup(groupId, group.CheckPoint - group.Center, maxSpeed);
			}
			else
			{
				if (world.TickIndex > group.ScaledAtTick + 400)
				{
					ScaleUnitsByGroup(groupId, 0.1, group.Center);
					group.DoNotMoveTill = LastMoveTick() + 20;
				}
				else
				{
					group.DoNotMoveTill = world.TickIndex + 5;
				}
			}
		}

		// Square of two rectangles intersection
		private double S(params Point[] p)
		{
			if (p.Count() != 4)
			{
				return -1;
			}
			if (p[3].X.Less(p[0].X) || p[1].X.Less(p[2].X) || p[3].Y.Less(p[0].Y) || p[1].Y.Less(p[2].Y))
			{
				return 0;
			}
			return p.Select(q => q.X).OrderBy(q => q).Skip(1).Take(2).Aggregate((q, w) => q - w) * p.Select(q => q.Y).OrderBy(q => q).Skip(1).Take(2).Aggregate((q, w) => q - w);
		}

		private double MaxAllowedSizeForGroup(VehiclesGroup group)
		{
			return Math.Sqrt(Math.Max(group.Vehicles.Where(q => q.IsAerial).Count(), group.Vehicles.Where(q => !q.IsAerial).Count())) * 7;
		}

		private double MaxAllowedSizeForGroup(int groupId)
		{
			return MaxAllowedSizeForGroup(myGroups[groupId]);
		}

		private bool IsOkCheckPointForGroup(int groupId, Point p)
		{
			var group = myGroups[groupId];
			if (p.Dist(group.Center).More(20))
			{
				var center = (group.Center + p) / 2;
				if (!IsOkCheckPointForGroup(groupId, center))
				{
					return false;
				}
			}
			if (p.X.Less(group.Center.X) && p.X.Less(32) || p.X.More(group.Center.X) && p.X.More(game.WorldWidth - 32) || p.Y.Less(group.Center.Y) && p.Y.Less(32) || p.Y.More(group.Center.Y) && p.Y.More(game.WorldWidth - 32))
			{
				return false;
			}
			foreach (var groupItem in myGroups.Where(q => q.Value.IsGround == group.IsGround))
			{
				if (groupItem.Key != groupId)
				{
					var group2 = groupItem.Value;
					double d1, d2;
					if (!group2.CheckPoint.Eq(MathMiXa.ZeroPoint))
					{
						var s = new Segment(group2.Center, group2.CheckPoint);
						d1 = s.Dist(group.Center);
						d2 = s.Dist(p);
					}
					else
					{
						d1 = group2.Center.Dist(group.Center);
						d2 = group2.Center.Dist(p);
					}
					var maxD = (MaxAllowedSizeForGroup(group) + MaxAllowedSizeForGroup(group2)) * 0.6;
					if (d2.Less(d1) && d2.Less(maxD))
					{
						return false;
					}
					if (d1.Less(maxD))
					{
						var newTopLeft = group.TopLeft + p - group.Center;
						var newBottomRight = group.BottomRight + p - group.Center;
						var rr = new Point(4, 4);
						double newS = S(group2.TopLeft - rr, group2.BottomRight + rr, newTopLeft - rr, newBottomRight + rr);
						double oldS = S(group2.TopLeft - rr, group2.BottomRight + rr, group.TopLeft - rr, group.BottomRight + rr);
						if (newS.More(oldS))
						{
							return false;
						}
					}
				}
			}
			return true;
		}

		private void AssignBestCheckPointForGroup(int groupId)
		{
			var step = groupStep;
			var group = myGroups[groupId];
			if (!group.IsGround)
			{
				step *= 2;
			}
			var dir = (group.Dest - group.Center).Normalize();
			var angle = Math.Sign(dir.Y) * Math.Acos(dir.X);
			double d = 0;
			while (d.Less(1.5))
			{
				d += 0.1;
				for (var i = -1; i < 2; i += 2)
				{
					var angleNew = angle + i * d;
					dir = new Point(Math.Cos(angleNew), Math.Sin(angleNew));
					var p = group.Center + dir * Math.Min(step / 2, group.Center.Dist(group.Dest));
					if (IsOkCheckPointForGroup(groupId, p))
					{
						group.CheckPoint = p;
						return;
					}
				}
			}
			group.CheckPoint = group.Center;
		}

		private void SwapGroupDest(int groupId)
		{
			var group = myGroups[groupId];
			if (group.IsGround && GetFacility(group.Dest) == null)
			{
				return;
			}
			foreach (var groupItem in myGroups.Where(q => q.Value.IsGround == group.IsGround))
			{
				var group2 = groupItem.Value;
				if (groupItem.Key != groupId && !group2.Dest.Eq(MathMiXa.ZeroPoint))
				{
					var d11 = group.Center.Dist(group.Dest);
					var d12 = group2.Center.Dist(group2.Dest);
					var d21 = group.Center.Dist(group2.Dest);
					var d22 = group2.Center.Dist(group.Dest);
					if ((d21 + d22).Less(d11 + d12))
					{
						var o = group.Dest;
						group.Dest = group2.Dest;
						group2.Dest = o;
					}
				}
			}
		}

		private double Bound(double q, double border = 32)
		{
			return Math.Max(border, Math.Min(game.WorldWidth - border, q));
		}

		private Point Bound(Point p, double border = 32)
		{
			return new Point(Bound(p.X, border), Bound(p.Y, border));
		}

		private bool CloseToBorder(Point p)
		{
			return p.X.LessEq(32) || p.X.MoreEq(world.Width - 32) || p.Y.LessEq(32) || p.Y.MoreEq(world.Height - 32);
		}

		private IEnumerable<Vehicle> GetDangerousVehicles(VehiclesGroup group, double radiusGround, double radiusAerial, Point p)
		{
			var types = new Dictionary<VehicleType, bool>();
			if (group.Count(VehicleType.Ifv) > group.Count() / 10)
			{
				types[VehicleType.Ifv] = true;
				types[VehicleType.Tank] = true;
				types[VehicleType.Helicopter] = true;
			}
			if (group.Count(VehicleType.Tank) > group.Count() / 10)
			{
				types[VehicleType.Tank] = true;
				types[VehicleType.Helicopter] = true;
			}
			if (group.Count(VehicleType.Arrv) > group.Count() / 2)
			{
				types[VehicleType.Tank] = true;
				types[VehicleType.Ifv] = true;
				types[VehicleType.Helicopter] = true;
			}
			if (group.Count(VehicleType.Fighter) > group.Count() / 10)
			{
				types[VehicleType.Fighter] = true;
				types[VehicleType.Ifv] = true;
			}
			if (group.Count(VehicleType.Helicopter) > group.Count() / 10)
			{
				types[VehicleType.Helicopter] = true;
				types[VehicleType.Fighter] = true;
				types[VehicleType.Ifv] = true;
			}
			if (group.Count(VehicleType.Fighter) > group.Count() / 3)
			{
				types[VehicleType.Helicopter] = false;
			}
			return hisVehicles.Where(q => q.Type.In(types.Where(w => w.Value).Select(w => w.Key).ToArray())).Where(q => !q.IsAerial && p.Dist(q).Less(radiusGround) || q.IsAerial && p.Dist(q).Less(radiusAerial));
		}

		private IEnumerable<Vehicle> GetDangerousVehicles(VehiclesGroup group, double radiusGround, double radiusAerial)
		{
			return GetDangerousVehicles(group, radiusGround, radiusAerial, group.Center);
		}

		private IEnumerable<Vehicle> GetDangerousVehicles(VehiclesGroup group, double radius, Point p)
		{
			return GetDangerousVehicles(group, radius, radius, p);
		}

		private IEnumerable<Vehicle> GetDangerousVehicles(VehiclesGroup group, double radius)
		{
			return GetDangerousVehicles(group, radius, radius, group.Center);
		}

		private IEnumerable<Vehicle> GetVictimVehicles(VehiclesGroup group, double radius, Point p)
		{
			var types = new Dictionary<VehicleType, bool>();
			if (group.Count(VehicleType.Ifv) > group.Count() / 10)
			{
				types[VehicleType.Ifv] = true;
				//types[VehicleType.Helicopter] = true;
				types[VehicleType.Arrv] = true;
			}
			if (group.Count(VehicleType.Tank) > group.Count() / 10)
			{
				types[VehicleType.Tank] = true;
				types[VehicleType.Ifv] = true;
				types[VehicleType.Arrv] = true;
			}
			if (group.Count(VehicleType.Fighter) > group.Count() / 10 && !group.IsGround)
			{
				types[VehicleType.Fighter] = true;
				types[VehicleType.Helicopter] = true;
			}
			if (group.Count(VehicleType.Helicopter) > group.Count() / 10)
			{
				types[VehicleType.Helicopter] = true;
				types[VehicleType.Tank] = true;
				types[VehicleType.Arrv] = true;
			}
			return hisVehicles.Where(q => q.Type.In(types.Keys.ToArray()) && p.Dist(q).Less(radius));
		}

		private IEnumerable<Vehicle> GetVictimVehicles(VehiclesGroup group, double radius)
		{
			return GetVictimVehicles(group, radius, group.Center);
		}

		private void SetNuclearGroup()
		{
			var aerialGroups = myGroups.Where(q => !q.Value.IsGround);
			if (aerialGroups.Count() > 0)
			{
				if (aerialGroups.Where(q => q.Value.Vehicles.Where(w => w.Type == VehicleType.Helicopter).Count() < 5).Count() > 0)
				{
					nuclearGroupId = aerialGroups.Where(q => q.Value.Vehicles.Where(w => w.Type == VehicleType.Helicopter).Count() < 5).First().Key;
				}
				else
				{
					nuclearGroupId = aerialGroups.OrderBy(q => bestNuclearPoint.p.Dist(q.Value.Center)).First().Key;
				}
			}
			else
			{
				var groundGroups = myGroups.Where(q => q.Value.IsGround);
				if (groundGroups.Count() > 0)
				{
					nuclearGroupId = groundGroups.OrderBy(q => bestNuclearPoint.p.Dist(q.Value.Center)).First().Key;
				}
				else
				{
					nuclearGroupId = 0;
				}
			}
		}

		private void SetBestDestForGroup(int groupId)
		{
			var group = myGroups[groupId];
			if (group.IsGround)
			{
				if (isTankSeparate && world.TickIndex < 1000 && group.Vehicles.First().Type == VehicleType.Tank && myGroups.Count() < 4)
				{
					if (vehiclesByType[VehicleType.Tank].Center.Between(MathMiXa.ZeroPoint, new Point(247, 247)))
					{
						if (tankGoDir == "vert")
						{
							group.Dest = new Point(myGroups[groupId].Center.X, 250);
							group.CheckPoint = group.Dest;
						}
						else
						{
							group.Dest = new Point(250, myGroups[groupId].Center.Y);
							group.CheckPoint = group.Dest;
						}
					}
					else
					{
						if (tankGoDir == "vert")
						{
							var facility = notMyFacilities.Where(q => q.Top.More(200)).OrderBy(q => group.Center.Dist(new Point(q)) * (q.Type == FacilityType.VehicleFactory ? 1 : 1.4)).First();
							group.Dest = new Point(facility);
						}
						else
						{
							var facility = notMyFacilities.Where(q => q.Left.More(200)).OrderBy(q => group.Center.Dist(new Point(q)) * (q.Type == FacilityType.VehicleFactory ? 1 : 1.4)).First();
							group.Dest = new Point(facility);
						}
					}
					return;
				}
				var suitableFacilities = notMyFacilities.Where(q => myGroups.Values.Where(w => w.Dest.Eq(new Point(q))).Count() == 0);
				if (suitableFacilities.Count() > 0)
				{
					var facility = suitableFacilities.OrderBy(q => group.Center.Dist(new Point(q)) * (q.Type == FacilityType.VehicleFactory ? 1 : 1.4)).First();
					group.Dest = new Point(facility);
				}
				else
				{
					suitableFacilities = myFacilities.Where(q => q.CapturePoints < game.MaxFacilityCapturePoints && myGroups.Values.Where(w => w.Dest.Eq(new Point(q))).Count() == 0);
					if (suitableFacilities.Count() > 0)
					{
						var facility = suitableFacilities.OrderBy(q => group.Center.Dist(new Point(q))).First();
						group.Dest = new Point(facility);
					}
					else
					{
						var dX = (game.WorldWidth - group.Center.X) / 10 - 1.0 * game.WorldWidth / 20;
						var dY = (game.WorldHeight - group.Center.Y) / 10 - 1.0 * game.WorldHeight / 20;
						do
						{
							group.Dest = new Point(Bound(group.Center.X + rnd.NextDouble() * 300 - 150 + dX), Bound(group.Center.Y + rnd.NextDouble() * 300 - 150 + dY));
						}
						while (CloseToBorder(group.Center) && CloseToBorder(group.Dest));
					}
				}
			}
			else
			{
				if (world.TickIndex > bestNuclearPoint.Tick + 60)
				{
					var np = FindBestNuclearPoint();
					if (np.damage > bestNuclearPoint.damage * 1.1 || world.TickIndex > bestNuclearPoint.Tick + 200 || np.p.Dist(bestNuclearPoint.p) < 50)
					{
						bestNuclearPoint = np;
					}
				}
				if (!myGroups.ContainsKey(nuclearGroupId) || 
					(myGroups[nuclearGroupId].IsGround && myGroups.Where(q => !q.Value.IsGround).Count() > 0) ||
					myGroups[nuclearGroupId].Vehicles.Where(q => q.Type == VehicleType.Helicopter).Count() > 10 && myGroups.Values.Where(q => !q.IsGround && !q.IsBuilding && q.Vehicles.Where(w => w.Type == VehicleType.Helicopter).Count() < 5).Count() > 0)
				{
					SetNuclearGroup();
				}
				if (game.IsFogOfWarEnabled && world.TickIndex < 2500 && hisVehicles.Where(q => q.Type != VehicleType.Fighter).Count() == 0 && myGroups.Values.Where(q => q.IsGround && q.Count() > 50).Count() == 3)
				{
					VehiclesGroup targetGroup = null;
					if (myGroups.Values.Where(q => q.IsGround && q.Count() > 50).Count() == 3)
					{
						var myLandGroups = myGroups.Values.Where(q => q.IsGround && q.Count() > 50);
						double minL = 100000000;
						foreach (var landGroup in myLandGroups)
						{
							double l = 0;
							foreach (var landGroup2 in myLandGroups)
							{
								l += landGroup.Center.Dist(landGroup2.Center);
							}
							if (l.Less(minL))
							{
								targetGroup = landGroup;
								minL = l;
							}
						}
					}
					else
					{
						targetGroup = myGroups.Values.Where(q => q.IsGround && q.Count() > 50).First();
					}
					if (world.TickIndex < 1300 && myGroups.Values.Where(q => q.IsGround && q.Isset(VehicleType.Tank)).Count() > 0)
					{
						targetGroup = myGroups.Values.Where(q => q.IsGround && q.Isset(VehicleType.Tank)).First();
					}
					if (targetGroup.Count(VehicleType.Arrv) > 10 && opponent.RemainingNuclearStrikeCooldownTicks > 150)
					{
						group.Dest = targetGroup.Center;
					}
					else
					{
						group.Dest = targetGroup.Center + (new Point(game.WorldWidth / 2, game.WorldHeight / 2) - targetGroup.Center).Normalize() * 70;
					}
				}
				else if (game.IsFogOfWarEnabled && isFacilities && hisFacilities.Where(q => q.VehicleType == VehicleType.Helicopter).Count() > 0 && group.Center.Dist(hisFacilities.Where(q => q.VehicleType == VehicleType.Helicopter).First()).More(20))
				{
					group.Dest = new Point(hisFacilities.Where(q => q.VehicleType == VehicleType.Helicopter).First());
				}
				else if (game.IsFogOfWarEnabled && isFacilities && hisFacilities.Where(q => q.VehicleType == VehicleType.Tank).Count() > 0 && group.Count(VehicleType.Helicopter) >= 10 && group.Center.Dist(hisFacilities.Where(q => q.VehicleType == VehicleType.Tank).First()).More(20))
				{
					group.Dest = new Point(hisFacilities.Where(q => q.VehicleType == VehicleType.Tank).First());
				}
				else if (game.IsFogOfWarEnabled && world.TickIndex < 1300)
				{
					if (isFacilities && facilities.Where(q => q.Type == FacilityType.VehicleFactory).Count() > 0)
					{
						var suitableFacilities = facilities.Where(q => q.Type == FacilityType.VehicleFactory).Where(q => new Point(q).Between(new Point(500, 500), new Point(1000, 1000))).OrderBy(q => new Point(1000, 1000).Dist(q));
						if (suitableFacilities.Count() > 0)
						{
							group.Dest = new Point(suitableFacilities.Last());
						}
						else
						{
							suitableFacilities = facilities.Where(q => q.Type == FacilityType.VehicleFactory).Where(q => !new Point(q).Between(MathMiXa.ZeroPoint, new Point(500, 500))).OrderBy(q => new Point(1000, 1000).Dist(q));
							group.Dest = new Point(suitableFacilities.First());
						}
					}
					else
					{
						var arrvs = myGroups.Values.Where(q => q.Vehicles.Where(w => w.Type == VehicleType.Arrv).Count() > 0);
						if (arrvs.Count() > 0)
						{
							group.Dest = (arrvs.First().Center * 2 + arrvs.First().Dest) / 3;
						}
						else
						{
							group.Dest = new Point(400, 400);
						}
					}
				}
				else if (nuclearGroupId == groupId)
				{
					var p = bestNuclearPoint.p;
					if (p.X.More(0) && p.Y.More(0))
					{
						var dir = new Point(0, 0);
						foreach (var vehicle in vehicleById.Select(q => q.Value).Where(q => (q.GetDistanceTo(p.X, p.Y) > 50 && q.GetDistanceTo(p.X, p.Y) < 240) || (groupId.In(q.Groups) && q.PlayerId == me.Id)))
						{
							double koef;
							if (vehicle.PlayerId == me.Id)
							{
								if (vehicle.Type == VehicleType.Fighter)
									koef = 1.2;
								else if (vehicle.Type == VehicleType.Helicopter)
									koef = -1;
								else
									koef = 0;
							}
							else if (vehicle.Type.In(VehicleType.Fighter, VehicleType.Ifv))
								koef = -1;
							else
								koef = 0;
							var q = vehicle - p;
							if (q.Dist() > 30)
							{
								dir += q.Normalize() / q.Dist() * koef;
							}
						}
						if (p.X < 150)
							dir += new Point(150 / (p.X + 1), 0);
						if (p.Y < 150)
							dir += new Point(0, 150 / (p.Y + 1));
						if (p.X > game.WorldWidth - 150)
							dir -= new Point(150 / (game.WorldWidth - p.X + 1), 0);
						if (p.Y > game.WorldHeight - 150)
							dir -= new Point(0, 150 / (game.WorldHeight - p.Y + 1));
						if (dir.X.Eq(0) && dir.Y.Eq(0))
						{
							dir = group.Center - p;
						}
						dir = dir.Normalize();
						var shift = 115;
						if (game.IsFogOfWarEnabled)
						{
							shift = 85;
						}
						while (VisionFactor(VehicleType.Fighter, p + dir * shift) * game.FighterVisionRange < shift)
						{
							shift -= 3;
						}
						p += dir * (shift - 3);
						var closestUnit = GetClosestUnit(p, group.Vehicles);
						dir = p - closestUnit;
						var dist = dir.Dist();
						if (dist.More(200))
						{
							dir = dir.Normalize() * 200;
						}
						if (dist.More(5))
						{
							group.Dest = group.Center + dir;
						}
						else if (bestNuclearPoint.p.Dist(group.Center) < shift - 2)
						{
							group.Dest = p;
						}
						else
						{
							group.Dest = group.Center;
							group.DoNotMoveTill = world.TickIndex + 30;
						}
					}
					else
					{
						var dX = (game.WorldWidth - group.Center.X) / 10 - 1.0 * game.WorldWidth / 20;
						var dY = (game.WorldHeight - group.Center.Y) / 10 - 1.0 * game.WorldHeight / 20;
						do
						{
							group.Dest = new Point(Bound(group.Center.X + rnd.NextDouble() * 400 - 200 + dX), Bound(group.Center.Y + rnd.NextDouble() * 400 - 200 + dY));
						}
						while (CloseToBorder(group.Center) && CloseToBorder(group.Dest));
					}
				}
				else
				{
					var dX = (game.WorldWidth - group.Center.X) / 10 - 1.0 * game.WorldWidth / 20;
					var dY = (game.WorldHeight - group.Center.Y) / 10 - 1.0 * game.WorldHeight / 20;
					do
					{
						group.Dest = new Point(Bound(group.Center.X + rnd.NextDouble() * 400 - 200 + dX), Bound(group.Center.Y + rnd.NextDouble() * 400 - 200 + dY));
					}
					while (CloseToBorder(group.Center) && CloseToBorder(group.Dest));
				}
			}
		}

		private int GetTotalHealth(IEnumerable<Vehicle> units)
		{
			var result = units.Select(q => q.Durability).Sum() - units.Where(q => q.Type == VehicleType.Arrv).Select(q => q.Durability).Sum();
			if (result > 0)
			{
				result += units.Count() * 10;
			}
			return result;
		}

		private int GetTotalHealth(VehiclesGroup group)
		{
			return GetTotalHealth(group.Vehicles);
		}

		private void GiveInstructionToGroup(int groupId)
		{
			SwapGroupDest(groupId);
			var group = myGroups[groupId];
			var step = groupStep;
			if (!group.IsGround)
			{
				step *= 2;
			}
			if (!game.IsFogOfWarEnabled)
			{
				var opp = GetClosestOpponent(group.Center, group.IsGround ? "land" : "all");
				if (opp == null || group.Center.Dist(opp).More(group.IsGround ? 150 : 250))
				{
					step += 20;
				}
			}
			var destIsSet = false;
			var facilityAtCenter = group.IsGround ? GetFacility(group.Center) : null;
			var facilityAtDest = group.IsGround ? GetFacility(group.Dest) : null;

			var maxSize = MaxAllowedSizeForGroup(group);
			if ((group.Width > maxSize || group.Height > maxSize) && (world.TickIndex > group.RotatedAtTick + 300 || world.TickIndex > group.ScaledAtTick + 300))
			{
				if (world.TickIndex > group.ScaledAtTick + 300 || world.TickIndex < 1200)
				{
					Point scaleP = group.Center;
					if (world.TickIndex > 1200)
					{
						scaleP += new Point(rnd.NextDouble() * maxSize / 2 - maxSize / 4, rnd.NextDouble() * maxSize / 2 - maxSize / 4);
					}
					ScaleUnitsByGroup(groupId, 0.1, scaleP);
					group.DoNotMoveTill = LastMoveTick() + (group.IsGround ? 40 : 15);
				}
				else
				{
					RotateUnitsByGroup(groupId, -3);
					group.DoNotMoveTill = LastMoveTick() + (group.IsGround ? 50 : 30);
				}
				//group.Dest = group.Center;
			}
			else
			{
				IEnumerable<Vehicle> dangerousVehicles;
				if (group.IsGround)
				{
					dangerousVehicles = GetDangerousVehicles(group, maxSize + 75);
				}
				else
				{
					dangerousVehicles = GetDangerousVehicles(group, maxSize, maxSize + 75);
				}
				IEnumerable<Vehicle> victimVehicles;
				if (!group.IsGround)
				{
					if (groupId == nuclearGroupId && world.TickIndex < 15000 && !game.IsFogOfWarEnabled)
					{
						if (group.Isset(VehicleType.Helicopter))
						{
							if (world.TickIndex < 3500 && group.Count(VehicleType.Helicopter) > 10 && opponentVehiclesByType[VehicleType.Helicopter].Count() + opponentVehiclesByType[VehicleType.Arrv].Count() > 20)
							{
								victimVehicles = GetVictimVehicles(group, 2000).Where(q => q.Type.In(VehicleType.Helicopter, VehicleType.Arrv)).Where(q => q.X.Less(game.WorldWidth / 2) || q.Y.Less(game.WorldHeight / 2));
							}
							else
							{
								victimVehicles = GetVictimVehicles(group, maxSize * (world.TickIndex < 3000 ? 5 : 2));
								if (victimVehicles.Where(q => q.Type != VehicleType.Fighter).Count() > 2)
								{
									victimVehicles = victimVehicles.Where(q => q.Type != VehicleType.Fighter);
								}
							}
						}
						else
						{
							victimVehicles = GetVictimVehicles(group, maxSize * 2);
							if (victimVehicles.Where(q => q.Type == VehicleType.Helicopter).Count() == 0 && victimVehicles.Where(q => q.Type == VehicleType.Fighter).Count() > 0)
							{
								victimVehicles = victimVehicles.Where(q => group.Center.Dist(q).Less(maxSize * (world.TickIndex > 10000 ? 2 : world.TickIndex > 3000 ? 1.5 : 1)));
							}
						}
					}
					else if (game.IsFogOfWarEnabled)
					{
						var types = new List<VehicleType>();
						if (group.Count(VehicleType.Fighter) >= 5)
						{
							types.Add(VehicleType.Helicopter);
						}
						if (group.Count(VehicleType.Helicopter) >= 5)
						{
							types.Add(VehicleType.Arrv);
						}
						victimVehicles = hisVehicles.Where(q => q.Type.In(types.ToArray()));
						if (victimVehicles.Count() < 4)
						{
							victimVehicles = GetVictimVehicles(group, 2000);
							if (hisHelicoptersTickCount < 1000 && world.TickIndex < 3000)
							{
								victimVehicles = victimVehicles.Where(q => q.Type != VehicleType.Fighter);
							}
							else if (victimVehicles.Count() < 5 && group.Count(VehicleType.Helicopter) > 10)
							{
								victimVehicles = hisVehicles;
							}
						}
					}
					else
					{
						victimVehicles = GetVictimVehicles(group, 2000);
						if (victimVehicles.Count() < 5 && group.Count(VehicleType.Helicopter) > 10)
						{
							victimVehicles = hisVehicles;
						}
					}
				}
				else
				{
					var suitableFacilities = notMyFacilities.Where(q => myGroups.Where(w => w.Key != groupId && w.Value.Dest.Eq(new Point(q))).Count() == 0);
					victimVehicles = GetVictimVehicles(group, suitableFacilities.Count() == 0 ? 2000 : maxSize);
				}

				if (world.TickIndex > 1200 && 
					GetTotalHealth(dangerousVehicles) > GetTotalHealth(group) * 1.3 &&
					(!group.IsGround || group.Center.Dist(group.Dest).More(250) || (facilityAtCenter == null || facilityAtCenter.OwnerPlayerId == me.Id)))
				{
					var dir = MathMiXa.ZeroPoint;
					var p = (group.Center - new VehiclesGroup(dangerousVehicles).Center);
					if (p.Dist2().More(0))
					{
						dir = p / p.Dist2();
					}
					p = (group.Center - new Point(group.Center.X, Math.Min(group.Center.Y - 1, 40)));
					dir += p / p.Dist2();
					p = (group.Center - new Point(group.Center.X, Math.Max(group.Center.Y + 1, world.Height - 40)));
					dir += p / p.Dist2();
					p = (group.Center - new Point(Math.Min(group.Center.X - 1, 40), group.Center.Y));
					dir += p / p.Dist2();
					p = (group.Center - new Point(Math.Max(group.Center.X + 1, world.Width - 40), group.Center.Y));
					dir += p / p.Dist2();
					group.Dest = Bound(group.Center + dir.Normalize() * (group.IsGround ? step : step / 2), maxSize / 2);
					destIsSet = true;
				}

				if (victimVehicles.Count() > 5 || world.TickIndex > 6000 || victimVehicles.Count() > 0 && hisVehicles.Where(q => q.Type != VehicleType.Fighter).Count() < 6)
				{
					Vehicle closestOpponent = null;
					if (victimVehicles.Count() > 0)
					{
						closestOpponent = victimVehicles.OrderBy(q => group.Center.Dist(q)).First();
					}
					if (closestOpponent != null && (!group.IsGround || group.Center.Dist(group.Dest).More(500) || GetFacility(group.Dest) == null))
					{
						dangerousVehicles = GetDangerousVehicles(group, maxSize / 2 + 25, new Point(closestOpponent));
						var dangerousHelicopters = dangerousVehicles.Where(q => q.Type == VehicleType.Helicopter);
						var dangerousGround = dangerousVehicles.Where(q => !q.IsAerial);
						if (GetTotalHealth(dangerousVehicles) < GetTotalHealth(group) || (!group.IsGround && world.TickIndex < 3000 && dangerousHelicopters.Count() > 2 && dangerousGround.Count() <= 10))
						{
							var closeVictims = new VehiclesGroup(victimVehicles.Where(q => closestOpponent.GetDistanceTo(q) < maxSize / 2));
							closestOpponent = closeVictims.Vehicles.OrderBy(q => closeVictims.Center.Dist(q)).First();
							if (group.Center.Dist(closestOpponent).More(step))
							{
								var p = new Point(closestOpponent);
								if (prevVehicleById.ContainsKey(closestOpponent.Id))
								{
									var dir = p - prevVehicleById[closestOpponent.Id];
									p += dir * 20;
								}
								group.Dest = Bound(group.Center + (p - group.Center).Normalize() * group.Center.Dist(p) / 2, maxSize / 2);
							}
							else
							{
								group.Dest = Bound(new Point(closestOpponent), maxSize / 2);
								if ((group.Dest.X.Eq(maxSize / 2) || group.Dest.X.Eq(game.WorldWidth - maxSize / 2)) && (group.Dest.Y.Eq(maxSize / 2) || group.Dest.Y.Eq(game.WorldWidth - maxSize / 2)))
								{
									group.Dest = new Point(closestOpponent);
									group.CheckPoint = group.Dest;
									MoveUnitsByGroupToCheckPoint(groupId);
									return;
								}
							}
							destIsSet = true;
						}
					}
				}
				if (!destIsSet && !group.IsGround)
				{
					dangerousVehicles = GetDangerousVehicles(group, maxSize);
					victimVehicles = GetVictimVehicles(group, maxSize);
					if (dangerousVehicles.Count() > 4 && victimVehicles.Count() == 0)
					{
						var dir = MathMiXa.ZeroPoint;
						var p = (group.Center - new VehiclesGroup(dangerousVehicles).Center);
						if (p.Dist2().More(0))
						{
							dir = p / p.Dist2();
						}
						p = (group.Center - new Point(group.Center.X, Math.Min(group.Center.Y - 1, 40)));
						dir += p / p.Dist2();
						p = (group.Center - new Point(group.Center.X, Math.Max(group.Center.Y + 1, world.Height - 40)));
						dir += p / p.Dist2();
						p = (group.Center - new Point(Math.Min(group.Center.X - 1, 40), group.Center.Y));
						dir += p / p.Dist2();
						p = (group.Center - new Point(Math.Max(group.Center.X + 1, world.Width - 40), group.Center.Y));
						dir += p / p.Dist2();
						group.Dest = Bound(group.Center + dir.Normalize() * (group.IsGround ? step : step / 2), maxSize / 2);
						destIsSet = true;
					}
				}
				if (!destIsSet && (facilityAtCenter == null || facilityAtCenter.OwnerPlayerId == me.Id))
				{
					var groupsToCombineWith = myGroups.Where(q => !q.Value.IsBuilding && q.Value.IsGround == group.IsGround && group.Center.Dist(q.Value.Center).Less(group.IsGround ? 250 : 600));
					if (world.TickIndex < 18000 && group.Count() < 0.006 * world.TickIndex && groupsToCombineWith.Count() > 1 && myGroups.Count > world.TickIndex / 2000)
					{
						var secondGroup = myGroups.Where(q => !q.Value.IsBuilding && q.Value.IsGround == group.IsGround).OrderBy(q => q.Value.Center.Dist(group.Center)).Skip(1).First();
						if (secondGroup.Value.Center.Dist(group.Center).Less(MaxAllowedSizeForGroup(secondGroup.Value) + 30))
						{
							var m1 = SelectUnitsByGroup(groupId);
							var m2 = QueueMove(new Move
							{
								Action = ActionType.Assign,
								Group = secondGroup.Key
							});
							var m3 = QueueMove(new Move
							{
								Action = ActionType.Dismiss,
								Group = groupId
							});
							QueueMoves(m1, m2, m3);
							secondGroup.Value.Dest = MathMiXa.ZeroPoint;
							secondGroup.Value.DoNotMoveTill = 0;
							return;
						}
						else
						{
							group.Dest = group.Center + (secondGroup.Value.Center - group.Center) / 3;
							if (group.Center.Dist(group.Dest).Less(step))
							{
								group.CheckPoint = group.Dest;
							}
							else
							{
								group.CheckPoint = group.Center + (group.Dest - group.Center).Normalize() * step;
							}
						}
						destIsSet = true;
					}
				}
				if (group.Dest.Eq(MathMiXa.ZeroPoint))
				{
					SetBestDestForGroup(groupId);
					destIsSet = true;
				}
				if (!destIsSet && group.IsGround)
				{
					if (group.Dest.CloseTo(group.Center, 3))
					{
						if (facilityAtCenter == null || facilityAtCenter.OwnerPlayerId == me.Id || facilityAtCenter.CapturePoints > 115 - group.Count() / 3)
						{
							SetBestDestForGroup(groupId);
							destIsSet = true;
						}
					}
					else
					{
						if ((facilityAtDest == null && group.Dest.CloseTo(group.Center, 10)) ||
							(facilityAtDest != null && facilityAtDest.OwnerPlayerId == me.Id && facilityAtDest.CapturePoints.Eq(game.MaxFacilityCapturePoints)))
						{
							SetBestDestForGroup(groupId);
							destIsSet = true;
						}
					}
				}
				if (!destIsSet && !group.IsGround)
				{
					if (group.Dest.CloseTo(group.Center, 6))
					{
						SetBestDestForGroup(groupId);
						destIsSet = true;
					}
				}
				if (!destIsSet && group.Dest.Eq(group.CheckPoint) && group.Dest.Eq(group.Center) && (!group.IsGround || facilityAtCenter == null || facilityAtCenter.OwnerPlayerId == me.Id))
				{
					SetBestDestForGroup(groupId);
					destIsSet = true;
				}
				if (world.TickIndex > 500 && !destIsSet && group.IsGround && facilityAtCenter != null && facilityAtCenter.OwnerPlayerId != me.Id && myGroups.Values.Where(q => q.Dest.Eq(new Point(facilityAtCenter))).Count() == 0)
				{
					group.Dest = new Point(facilityAtCenter);
					destIsSet = true;
				}
				if (world.TickIndex - group.StartTick > 10 && (!group.IsGround || facilityAtCenter == null || facilityAtCenter.OwnerPlayerId == me.Id))
				{
					if (group.Center.Eq(group.CheckPoint) && world.TickIndex > Math.Max(1500, group.TickMoved + groupMoveInterval))
					{
						SetBestDestForGroup(groupId);
					}
					else if (world.TickIndex > 1000 && group.StartPoint.Dist(group.Center).Less((world.TickIndex - group.StartTick) * (group.IsGround ? 0.15 : 0.4)) &&
						(!group.IsGround || GetFacility(group.Dest) == null || GetFacility(group.Dest).OwnerPlayerId == me.Id))
					{
						SetBestDestForGroup(groupId);
					}
				}
				SwapGroupDest(groupId);
				if (!group.Dest.Eq(group.CheckPoint))
				{
					if (group.Center.Dist(group.Dest).Less(step))
					{
						group.CheckPoint = group.Dest;
					}
					else
					{
						group.CheckPoint = group.Center + (group.Dest - group.Center).Normalize() * step;
					}
					if (!IsOkCheckPointForGroup(groupId, group.CheckPoint))
					{
						AssignBestCheckPointForGroup(groupId);
					}
				}
				if (world.TickIndex < 1500 && myGroups.Count() < 4 && !group.IsGround && group.Dest.Between(MathMiXa.ZeroPoint, new Point(260, 260)))
				{
					group.Dest = new Point(260, 260);
					group.CheckPoint = group.Dest;
				}
				MoveUnitsByGroupToCheckPoint(groupId);
			}
		}

		private void DoNuclearStrike()
		{
			lastNuclearStrikeAttempt = world.TickIndex;
			if (!myGroups.ContainsKey(nuclearGroupId) || (myGroups[nuclearGroupId].IsGround && myGroups.Values.Where(q => !q.IsBuilding && !q.IsGround).Count() > 0))
			{
				SetNuclearGroup();
			}
			if (nuclearGroupId == 0 || (lastNuclearStrikeAvoidGroupId == nuclearGroupId && world.TickIndex < lastNuclearStrikeAvoid + 55))
			{
				return;
			}
			NuclearPoint bestNuclearP = new NuclearPoint(new Point(0, 0), 0);
			foreach (var group in myGroups.Values)
			{
				var points = GetVisiblePoints(group, 4);
				var nuclearP = FindBestNuclearPoint(points);
				if (nuclearP.damage > bestNuclearP.damage)
				{
					bestNuclearP = nuclearP;
				}
			}
			var p = bestNuclearP.p;
			if (p.X.More(0) && p.Y.More(0) && (world.TickIndex >= 2000 && bestNuclearP.damage > (game.IsFogOfWarEnabled ? myVehicles.Count() : hisVehicles.Count()) * 2 || world.TickIndex < 2000 && bestNuclearP.damage > 300))
			{
				var aimers = myVehicles.Where(q => p.Dist(new Point(q)).Less(VisionFactor(q) * VisionRangeByType(q.Type) - 2));
				if (aimers.Count() > 0)
				{
					var aimer = aimers.Last();
					var movesCount = 0;
					if (aimer.Groups.Length > 0)
					{
						moves.ClearByGroup(aimer.Groups[0]);
						MoveUnitsByGroup(aimer.Groups[0], new Point(0, 0));
						movesCount++;
					}
					QueueMoves(new Move
					{
						Action = ActionType.TacticalNuclearStrike,
						X = p.X,
						Y = p.Y,
						VehicleId = aimer.Id
					});
					movesCount++;
					moves.BringToFrontLast(movesCount);
					if (aimer.Groups.Length > 0 && myGroups.ContainsKey(aimer.Groups[0]))
					{
						myGroups[aimer.Groups[0]].Dest = myGroups[aimer.Groups[0]].Center;
						myGroups[aimer.Groups[0]].DoNotMoveTill = world.TickIndex + game.TacticalNuclearStrikeDelay + 1;
					}
				}
			}
			else
			{
				lastNuclearStrikeAttempt += 50;
			}
		}

		private void AvoidNuclearStrike()
		{
			if (moves.GetFutureMoves().Where(q => q.Action == ActionType.TacticalNuclearStrike).Count() > 0)
			{
				return;
			}
			var p = new Point(opponent.NextNuclearStrikeX, opponent.NextNuclearStrikeY);
			var closeUnits = myVehicles.Where(q => q.GetDistanceTo(p.X, p.Y).LessEq(2));
			if (closeUnits.Count() > 0)
			{
				p = new Point(closeUnits.First()) + new Point(2, 2);
			}
			var vehiclesNuked = myVehicles.Where(q => q.GetDistanceTo(p.X, p.Y).LessEq(game.TacticalNuclearStrikeRadius + 2));
			foreach (var group in myGroups)
			{
				if (vehiclesNuked.Where(q => group.Key.In(q.Groups)).Count() > 0)
				{
					if (me.NextNuclearStrikeTickIndex > 0 && group.Value.Vehicles.Where(q => q.Id == me.NextNuclearStrikeVehicleId).Count() > 0)
					{
						moves.ClearByGroup(group.Key);
						var m1 = SelectUnitsByGroup(group.Key);
						var m2 = QueueMove(new Move
						{
							Action = ActionType.Deselect,
							Top = vehicleById[me.NextNuclearStrikeVehicleId].Y - 2,
							Left = vehicleById[me.NextNuclearStrikeVehicleId].X - 2,
							Right = vehicleById[me.NextNuclearStrikeVehicleId].X + 2,
							Bottom = vehicleById[me.NextNuclearStrikeVehicleId].Y + 2,
						});
						var m3 = QueueMove(new Move
						{
							Action = ActionType.Scale,
							Factor = 10,
							X = p.X,
							Y = p.Y
						});
						QueueMoves(m1, m2, m3);
					}
					else
					{
						ScaleUnitsByGroup(group.Key, 10, p);
					}
					ScaleUnitsByGroup(group.Key, 0.1, p, 5, opponent.NextNuclearStrikeTickIndex);
					moves.BringToFrontLast(2);
					group.Value.DoNotMoveTill = opponent.NextNuclearStrikeTickIndex + 30;
					lastNuclearStrikeAvoid = world.TickIndex;
					lastNuclearStrikeAvoidGroupId = group.Key;
				}
			}
		}

		private void ProcessFactories()
		{
			var factories = myFacilities.Where(q => q.Type == FacilityType.VehicleFactory);
			foreach (var factory in factories.Where(q => facilitiesAdd[q.Id].ProductionStarted == 0))
			{
				var aerialFactories = factories.Where(q => q.Id != factory.Id && !facilitiesAdd[q.Id].IsGround);
				if (myGroups.Values.Where(q => q.IsGround).Count() < 10 && myGroups.Values.Where(q => !q.IsGround && !q.IsBuilding).Count() + aerialFactories.Count() > 0)
				{
					SetFactoryVehicleType(factory, VehicleType.Tank);
				}
				else
				{
					if (opponentVehiclesByType[VehicleType.Fighter].Count() > vehiclesByType[VehicleType.Fighter].Count())
					{
						SetFactoryVehicleType(factory, VehicleType.Fighter);
					}
					else
					{
						SetFactoryVehicleType(factory, VehicleType.Helicopter);
					}
				}
			}
			foreach (var factory in factories.Where(q => q.ProductionProgress > 0 && facilitiesAdd[q.Id].ProductionStarted > 0 && world.TickIndex > facilitiesAdd[q.Id].ProductionChanged + (facilitiesAdd[q.Id].IsGround ? 900 : 1100)))
			{
				var vehiclesInside = myVehicles.Where(q => q.IsAerial != facilitiesAdd[factory.Id].IsGround && q.X.Between(factory.Left, factory.Left + game.FacilityWidth) && q.Y.Between(factory.Top, factory.Top + game.FacilityHeight));
				if (vehiclesInside.Count() > 0 && vehiclesInside.Count() < 10)
				{
					var types = vehiclesInside.Select(q => q.Type).Distinct();
					if (types.Count() == 0)
					{
						continue;
					}
					if (types.First() == VehicleType.Helicopter)
					{
						SetFactoryVehicleType(factory, VehicleType.Fighter);
					}
					if (types.First() == VehicleType.Tank)
					{
						SetFactoryVehicleType(factory, VehicleType.Ifv);
					}
					facilitiesAdd[factory.Id].ProductionChanged = LastMoveTick();
				}
			}
			foreach (var factory in factories.Where(q => q.ProductionProgress == 0 && facilitiesAdd[q.Id].ProductionStarted > 0))
			{
				var vehiclesInside = myVehicles.Where(q => q.IsAerial != facilitiesAdd[factory.Id].IsGround && q.X.Between(factory.Left, factory.Left + game.FacilityWidth) && q.Y.Between(factory.Top, factory.Top + game.FacilityHeight));
				if (vehiclesInside.Count() > 0)
				{
					var vehiclesInsideNotGroupped = vehiclesInside.Where(q => q.Groups.Count() == 0);
					if (vehiclesInsideNotGroupped.Count() > 8)
					{
						var types = vehiclesInsideNotGroupped.Select(q => q.Type).Distinct();
						if (types.Count() == 0)
						{
							continue;
						}
						var groupId = 0;
						if (vehiclesInsideNotGroupped.Count() == vehiclesInside.Count())
						{
							groupId = NextGroupId;
						}
						else if (vehiclesInside.Where(q => q.Groups.Count() > 0).Select(q => q.Groups[0]).Distinct().Count() == 1)
						{
							groupId = vehiclesInside.Where(q => q.Groups.Count() > 0).Select(q => q.Groups[0]).Distinct().First();
						}
						if (groupId > 0)
						{
							var m1 = new List<Move>();
							foreach (var type in types)
							{
								m1.Add(QueueMove(new Move
								{
									Action = m1.Count == 0 ? ActionType.ClearAndSelect : ActionType.AddToSelection,
									Left = factory.Left,
									Right = factory.Left + game.FacilityWidth,
									Top = factory.Top,
									Bottom = factory.Top + game.FacilityHeight,
									VehicleType = type
								}));
							}
							var m2 = QueueMove(new Move
							{
								Action = ActionType.Assign,
								Group = groupId
							});
							if (m1.Count() == 1)
							{
								QueueMoves(m1[0], m2);
							}
							else
							{
								QueueMoves(m1[0], m1[1], m2);
							}
							moves.BringToFrontLast();
						}
						if (myGroups.ContainsKey(groupId) && (myGroups[groupId].Count() > (myGroups[groupId].IsGround ? 33 : 16) || world.TickIndex > 17000))
						{
							myGroups[groupId].IsBuilding = false;
							ScaleUnitsByGroup(groupId, 0.1, myGroups[groupId].Center);
							myGroups[groupId].DoNotMoveTill = LastMoveTick() + 30;
							var aerialFactories = factories.Where(q => q.Id != factory.Id && !facilitiesAdd[q.Id].IsGround);
							if (myGroups.Values.Where(q => q.IsGround).Count() < 10 && myGroups.Values.Where(q => !q.IsGround && !q.IsBuilding).Count() + aerialFactories.Count() > 1)
							{
								SetFactoryVehicleType(factory, VehicleType.Tank);
							}
							else
							{
								if (opponentVehiclesByType[VehicleType.Fighter].Count() > vehiclesByType[VehicleType.Fighter].Count())
								{
									SetFactoryVehicleType(factory, VehicleType.Fighter);
								}
								else
								{
									SetFactoryVehicleType(factory, VehicleType.Helicopter);
								}
							}
						}
						else
						{
							ScaleUnitsByGroup(groupId, 0.1, new Point(factory));
							if (facilitiesAdd[factory.Id].IsGround)
							{
								if (myGroups.ContainsKey(groupId) && (myGroups[groupId].Count().Between(5, 15)))
								{
									SetFactoryVehicleType(factory, VehicleType.Ifv);
								}
								else if (factory.VehicleType != VehicleType.Tank)
								{
									SetFactoryVehicleType(factory, VehicleType.Tank);
								}
							}
							else
							{
								if (types.First() == VehicleType.Fighter)
								{
									SetFactoryVehicleType(factory, VehicleType.Helicopter);
								}
								else
								{
									SetFactoryVehicleType(factory, VehicleType.Fighter);
								}
							}
						}
					}
				}
			}
			if (world.TickIndex % 70 == 1)
			{
				factories = notMyFacilities.Where(q => q.Type == FacilityType.VehicleFactory);
				foreach (var factory in factories)
				{
					var vehiclesInside = myVehicles.Where(q => q.X.Between(factory.Left, factory.Left + game.FacilityWidth) && q.Y.Between(factory.Top, factory.Top + game.FacilityHeight));
					if (vehiclesInside.Count() > 0)
					{
						var vehiclesInsideNotGroupped = vehiclesInside.Where(q => q.Groups.Count() == 0);
						if (vehiclesInsideNotGroupped.Count() > 0)
						{
							var types = vehiclesInsideNotGroupped.Select(q => q.Type).Distinct();
							if (types.Count() == 0)
							{
								continue;
							}
							var groupId = 0;
							if (vehiclesInsideNotGroupped.Count() == vehiclesInside.Count())
							{
								groupId = NextGroupId;
							}
							else if (vehiclesInside.Where(q => q.IsAerial == vehiclesInsideNotGroupped.First().IsAerial && q.Groups.Count() > 0).Select(q => q.Groups[0]).Distinct().Count() == 1)
							{
								groupId = vehiclesInside.Where(q => q.Groups.Count() > 0).Select(q => q.Groups[0]).Distinct().First();
							}
							if (groupId > 0)
							{
								var m1 = new List<Move>();
								foreach (var type in types)
								{
									m1.Add(QueueMove(new Move
									{
										Action = m1.Count == 0 ? ActionType.ClearAndSelect : ActionType.AddToSelection,
										Left = factory.Left,
										Right = factory.Left + game.FacilityWidth,
										Top = factory.Top,
										Bottom = factory.Top + game.FacilityHeight,
										VehicleType = type
									}));
								}
								var m2 = QueueMove(new Move
								{
									Action = ActionType.Assign,
									Group = groupId
								});
								if (m1.Count() == 1)
								{
									QueueMoves(m1[0], m2);
								}
								else
								{
									QueueMoves(m1[0], m1[1], m2);
								}
								moves.BringToFrontLast();
							}
						}
					}
				}
			}
		}

		private void MakeMove()
		{
			MakeFormation();
			if (!isFacilities && Math.Abs(tickHistory["HisNuclear"] - world.TickIndex) < game.TacticalNuclearStrikeDelay - 1)
			{
				return;
			}
			if (!isFacilities)
			{
				if (myArmy.Count() > 0)
				{
					var closestUnit = GetClosestOpponent(armyCenter);
					if (me.NextNuclearStrikeTickIndex == -1 && me.RemainingNuclearStrikeCooldownTicks == 0 && world.TickIndex % 20 == 0 &&
						closestUnit.GetDistanceTo(armyCenter.X, armyCenter.Y).Less(75) && !vehiclesByType[VehicleType.Fighter].Isset() && hisVehicles.Count() > 0)
					{
						var points = GetVisiblePoints(8);
						NuclearPoint nuclearP = FindBestNuclearPoint(points);
						Point p = nuclearP.p;
						var aimers = myVehicles.Where(q => p.Dist(new Point(q)).Less(VisionFactor(q) * VisionRangeByType(q.Type) - 2));
						if (p.X.More(0) && p.Y.More(0) && aimers.Count() > 0)
						{
							moves.Clear();
							QueueMoves(new Move
							{
								Action = ActionType.TacticalNuclearStrike,
								X = p.X,
								Y = p.Y,
								VehicleId = aimers.Last().Id
							});
						}
					}
				}
				if (opponent.NextNuclearStrikeTickIndex - world.TickIndex == game.TacticalNuclearStrikeDelay - 1)
				{
					if (world.TickIndex > 1500)
					{
						moves.Clear();
					}
					tickHistory["HisNuclear"] = opponent.NextNuclearStrikeTickIndex;
					var p = new Point(opponent.NextNuclearStrikeX, opponent.NextNuclearStrikeY);
					var closeUnits = myVehicles.Where(q => q.GetDistanceTo(p.X, p.Y).LessEq(2));
					if (closeUnits.Count() > 0)
					{
						p = new Point(closeUnits.First()) + new Point(2, 2);
					}
					var vehiclesNuked = myVehicles.Where(q => q.GetDistanceTo(p.X, p.Y).LessEq(game.TacticalNuclearStrikeRadius + 2));
					if (vehiclesNuked.Where(q => !q.IsAerial).Count() > 0)
					{
						ScaleUnitsByGroup(Group.ARMY, 10, p);
						ScaleUnitsByGroup(Group.ARMY, 0.1, p, 5, opponent.NextNuclearStrikeTickIndex);
						MoveUnitsByGroup(Group.ARMY, new Point(0, 0), 5, opponent.NextNuclearStrikeTickIndex + game.TacticalNuclearStrikeDelay - 1); // Stop scaling
						moves.BringToFrontLast(3);
					}
				}
				if (world.TickIndex > 2000 && world.TickIndex % 100 == 0 && tickHistory["GoArmy"] == world.TickCount)
				{
					var opponentsCloseToArmy = hisVehicles.Where(q => GetClosestUnit(q, myArmy).GetDistanceTo(q).Less(150));
					var opponentsCloseToArmyByLand = opponentsCloseToArmy.Where(q => !q.IsAerial);
					if (opponentsCloseToArmyByLand.Count() > 0 || me.Score <= opponent.Score || me.Score < 200 && world.TickIndex > 9000)
					{
						tickHistory["GoArmy"] = 0;
					}
				}
				if (world.TickIndex > 2000 && !vehiclesByType[VehicleType.Fighter].Isset() && tickHistory["GoArmy"] == world.TickCount)
				{
					tickHistory["GoArmy"] = 0;
				}
				if (world.TickIndex > tickHistory["GoArmy"] && myArmy.Count() > 0)
				{
					var armyUnitsByLand = myArmy.Where(q => !q.IsAerial);
					var armyUnitFarthestFromCenter = GetFarthestUnit(armyCenter, myArmy);
					var armyUnitsCloseToCenter = myArmy.Where(q => q.GetDistanceTo(armyCenter.X, armyCenter.Y).Less(20));
					var armyUnitsCloseToCenterByLand = armyUnitsCloseToCenter.Where(q => !q.IsAerial);
					var opponentsAttackingArmy = hisVehicles.Where(q => GetClosestUnit(q, myArmy).GetDistanceTo(q).Less(20));
					var opponentsAttackingArmyByLand = opponentsAttackingArmy.Where(q => !q.IsAerial);
					var opponentsCloseToArmy = hisVehicles.Where(q => GetClosestUnit(q, myArmy).GetDistanceTo(q).Less(50));
					var opponentsCloseToArmyByLand = opponentsCloseToArmy.Where(q => !q.IsAerial);
					Vehicle opponentClosestToArmy = null;
					if (hisVehiclesLand.Count() > 0)
					{
						opponentClosestToArmy = GetClosestOpponent(armyCenter, "land");
					}
					else
					{
						opponentClosestToArmy = GetClosestOpponent(armyCenter);
					}
					if (opponent.RemainingNuclearStrikeCooldownTicks > 150 || opponentsAttackingArmyByLand.Count() > 5)
					{
						Point p;
						if (armyCenter.Dist(armyUnitFarthestFromCenter) > 50 + armyUnitsByLand.Count() / 10)
						{
							if (me.Score <= opponent.Score && opponent.Score > 10 && armyCenter.Dist(opponentClosestToArmy).Less(200))
							{
								p = new Point(GetClosestUnit(opponentClosestToArmy, myArmy));
							}
							else
							{
								p = armyCenter;
							}
							ScaleUnitsByGroup(Group.ARMY, 0.1, p);
							tickHistory["GoArmy"] = LastMoveTick() + 150;
						}
						else
						{
							var realScore = me.Score - (world.TickCount - Math.Max(world.TickIndex, 10000)) / 1000 * 20;
							if (me.Score <= opponent.Score && opponent.Score > 10 || world.TickIndex > 9000 && realScore < 0)
							{
								MoveUnitsByGroup(Group.ARMY, new Point(opponentClosestToArmy) - armyCenter, 0.18);
							}
							else if (armyCenter.X.More(121) || armyCenter.Y.More(121))
							{
								MoveUnitsByGroup(Group.ARMY, new Point(Math.Min(120, armyCenter.X), Math.Min(120, armyCenter.Y)) - armyCenter, 0.18);
							}
							tickHistory["GoArmy"] = LastMoveTick() + 50;
						}
					}
					else
					{
						if (armyCenter.Dist(armyUnitFarthestFromCenter) < 55 + armyUnitsByLand.Count() / 10)
						{
							ScaleUnitsByGroup(Group.ARMY, 1.6, armyCenter);
							tickHistory["GoArmy"] = LastMoveTick() + 150;
						}
						else
						{
							var realScore = me.Score - (world.TickCount - Math.Max(world.TickIndex, 10000)) / 1000 * 20;
							if (me.Score <= opponent.Score && opponent.Score > 10 || world.TickIndex > 9000 && realScore < 0)
							{
								MoveUnitsByGroup(Group.ARMY, new Point(opponentClosestToArmy) - armyCenter, 0.18);
							}
							else if (armyCenter.X.More(121) || armyCenter.Y.More(121))
							{
								MoveUnitsByGroup(Group.ARMY, new Point(Math.Min(120, armyCenter.X), Math.Min(120, armyCenter.Y)) - armyCenter, 0.18);
							}
							tickHistory["GoArmy"] = LastMoveTick() + 50;
						}
					}
				}
			}
			if ((myGroups.Count() > 0 || world.TickIndex > 1500))
			{
				if ((world.TickIndex > 1500 || myGroups.Count > 3) && isFacilities)
				{
					ProcessFactories();
				}
				if (opponent.NextNuclearStrikeTickIndex > world.TickIndex + 1 && world.TickIndex > lastNuclearStrikeAvoid + 50)
				{
					tickHistory["HisNuclear"] = opponent.NextNuclearStrikeTickIndex;
					AvoidNuclearStrike();
				}
				if (me.RemainingNuclearStrikeCooldownTicks <= 1 && me.NextNuclearStrikeTickIndex < world.TickIndex && world.TickIndex > lastNuclearStrikeAttempt + 30 && hisVehicles.Count > 0)
				{
					DoNuclearStrike();
				}
				if (me.NextNuclearStrikeTickIndex >= world.TickIndex && nuclearGroupId > 0 && myGroups.ContainsKey(nuclearGroupId))
				{
					myGroups[nuclearGroupId].DoNotMoveTill = me.NextNuclearStrikeTickIndex + 1;
				}
				foreach (var group in myGroups.Where(q => q.Key >= firstGroupId))
				{
					if (group.Value.Center.CloseTo(group.Value.CheckPoint, 3) ||
						group.Value.Center.CloseTo(group.Value.CheckPoint, 6) && !group.Value.IsGround ||
						group.Value.CheckPoint.Eq(MathMiXa.ZeroPoint) || 
						world.TickIndex > group.Value.TickMoved + groupMoveInterval)
					{
						if (world.TickIndex >= group.Value.DoNotMoveTill && !moves.IssetFutureMoveByGroup(group.Key) && !group.Value.IsBuilding)
						{
							GiveInstructionToGroup(group.Key);
						}
					}
				}
				foreach (var group in myGroups.Values.Where(q => q.IsBuilding && (GetFacility(q.Center) == null || GetFacility(q.Center).OwnerPlayerId != me.Id)))
				{
					group.IsBuilding = false;
				}
			}
		}

		public void Move(Player _me, World _world, Game _game, Move _move)
		{
			Init(_me, _world, _game, _move);
#if DEBUG
			vc.BeginPost();
			foreach (var group in myGroups.Where(q => q.Key >= firstGroupId))
			{
				var g = group.Value;
				vc.Line(g.Center.X, g.Center.Y, g.Dest.X, g.Dest.Y);
				vc.Line(g.Center.X, g.Center.Y, g.CheckPoint.X, g.CheckPoint.Y, 1, 0, 0);
				vc.Text(g.Center.X + 30, g.Center.Y - 30, group.Key.ToString());
			}
#endif
			if (me.RemainingActionCooldownTicks > 0)
			{
				return;
			}
			MakeMove();
			ExecuteDelayedMove();
#if DEBUG
			vc.EndPost();
#endif
		}
	}

	public class FacilityAdd
	{
		public int ProductionStarted { get; set; }
		public int ProductionChanged { get; set; }
		public bool IsGround { get; set; }
	}

	public static class Group
	{
		public const int ARMY = 1;
		public const int FIGHTERS = 2;
		public const int Count = 2;
	}

	public enum Action
	{
		None,
		NuclearPosition,
	}

	public class Behaviour
	{
		public Action action;
		public Point coord;
		public Behaviour()
		{
			action = Action.None;
			coord = new Point(0, 0);
		}
	}

	public struct Point
	{

		public double X, Y;
		public Point(double x, double y)
		{
			this.X = x;
			this.Y = y;
		}
		public Point(Vehicle q)
		{
			this.X = q.X;
			this.Y = q.Y;
		}
		public Point(Facility f)
		{
			this.X = f.Left + 32;
			this.Y = f.Top + 32;
		}
		public override string ToString()
		{
			return X.ToString() + ' ' + Y.ToString();
		}
		public static Point operator +(Point q, Point w)
		{
			return new Point(q.X + w.X, q.Y + w.Y);
		}
		public static Point operator +(Vehicle q, Point w)
		{
			return new Point(q.X + w.X, q.Y + w.Y);
		}
		public static Point operator +(Point q, Vehicle w)
		{
			return new Point(q.X + w.X, q.Y + w.Y);
		}
		public static Point operator -(Point q, Point w)
		{
			return new Point(q.X - w.X, q.Y - w.Y);
		}
		public static Point operator -(Vehicle q, Point w)
		{
			return new Point(q.X - w.X, q.Y - w.Y);
		}
		public static Point operator -(Point q, Vehicle w)
		{
			return new Point(q.X - w.X, q.Y - w.Y);
		}
		public static Point operator *(Point q, double w)
		{
			return new Point(q.X * w, q.Y * w);
		}
		public static Point operator /(Point q, double w)
		{
			return new Point(q.X / w, q.Y / w);
		}
		public static double operator *(Point q, Point w)
		{
			return q.X * w.X + q.Y * w.Y;
		}
		public static bool operator >(Point q, Point w)
		{
			return q.X.More(w.X) && q.Y.More(w.Y);
		}
		public static bool operator <(Point q, Point w)
		{
			return q.X.Less(w.X) && q.Y.Less(w.Y);
		}
		public bool Eq(Point p)
		{
			return this.Dist(p).Eq(0);
		}
		public bool Between(Point q, Point w)
		{
			return this.X.Between(q.X, w.X) && this.Y.Between(q.Y, w.Y);
		}
		public double Dist()
		{
			return Math.Sqrt(this.X * this.X + this.Y * this.Y);
		}
		public double Dist(Point p)
		{
			var q = this - p;
			return q.Dist();
		}
		public double Dist(Vehicle v)
		{
			return Dist(new Point(v));
		}
		public double Dist(Facility f)
		{
			return Dist(new Point(f));
		}
		// Distance to the line p1-p2
		public double Dist(Point p1, Point p2)
		{
			double r = p1.Dist(p2);
			return Math.Abs(MathMiXa.SSignedDoubled(this, p1, p2)) / r;
		}
		public double Dist(Segment s)
		{
			return s.Dist(this);
		}
		public double Dist2()
		{
			return this.X * this.X + this.Y * this.Y;
		}
		public Point Normalize()
		{
			var d = Dist();
			return new Point(this.X / d, this.Y / d);
		}
		public bool CloseTo(Point p, double d = 0.1)
		{
			return this.Dist(p).Less(d);
		}
	}

	public struct Segment
	{
		public Point P1, P2;
		public Segment(Point p, Point q)
		{
			P1 = p;
			P2 = q;
		}
		public double Dist(Point p)
		{
			Point v = this.P2 - this.P1;
			Point w = p - this.P1;
			double c1 = w * v;
			if (c1 <= 0)
			{
				return (p - this.P1).Dist();
			}
			double c2 = v * v;
			if (c2 <= c1)
			{
				return (p - this.P2).Dist();
			}
			double b = c1 / c2;
			Point Pb = this.P1 + v * b;
			return (p - Pb).Dist();
		}
	}

	public struct Line
	{
		public double A, B, C;
		public Line(double a, double b, double c)
		{
			A = a;
			B = b;
			C = c;
		}
		public override string ToString()
		{
			return string.Format("{0} {1} {2}", A, B, C);
		}

		public Point Cross(Line l)
		{
			double zn = MathMiXa.Determinant(A, B, l.A, l.B);
			if (Math.Abs(zn) < MathMiXa.Eps)
			{
				return new Point(1000000, 1000000);
				throw new ArgumentException();
			}
			return new Point(-MathMiXa.Determinant(C, B, l.C, l.B) / zn, -MathMiXa.Determinant(A, C, l.A, l.C) / zn);
		}
	}

	public class VehiclesGroup
	{
		private List<Vehicle> _vehicles;
		private Point _dest = MathMiXa.ZeroPoint;
		private Point _checkpoint = MathMiXa.ZeroPoint;

		public VehiclesGroup(List<Vehicle> vehicles)
		{
			UpdateVehicles(vehicles);
		}
		public VehiclesGroup(IEnumerable<Vehicle> vehicles) : this(vehicles.ToList())
		{
		}
		public void UpdateVehicles(List<Vehicle> vehicles)
		{
			_vehicles = vehicles;
			var x = vehicles.Select(q => q.X).DefaultIfEmpty().Average();
			var y = vehicles.Select(q => q.Y).DefaultIfEmpty().Average();
			var xMin = vehicles.Select(q => q.X).DefaultIfEmpty().Min();
			var yMin = vehicles.Select(q => q.Y).DefaultIfEmpty().Min();
			var xMax = vehicles.Select(q => q.X).DefaultIfEmpty().Max();
			var yMax = vehicles.Select(q => q.Y).DefaultIfEmpty().Max();
			Center = new Point(x, y);
			TopLeft = new Point(xMin, yMin);
			BottomRight = new Point(xMax, yMax);
			TopRight = new Point(BottomRight.X, TopLeft.Y);
			BottomLeft = new Point(TopLeft.X, BottomRight.Y);
			Width = BottomRight.X - TopLeft.X;
			Height = BottomRight.Y - TopLeft.Y;
			IsGround = vehicles.Where(q => !q.IsAerial).Count() > 0;
		}
		public void UpdateVehicles(IEnumerable<Vehicle> vehicles)
		{
			UpdateVehicles(vehicles.ToList());
		}
		public int Count()
		{
			return _vehicles.Count;
		}
		public int Count(VehicleType t)
		{
			return _vehicles.Where(q => q.Type == t).Count();
		}
		public Point Center { get; private set; }
		public Point TopLeft { get; private set; }
		public Point BottomRight { get; private set; }
		public Point TopRight { get; private set; }
		public Point BottomLeft { get; private set; }
		public double Width { get; private set; }
		public double Height { get; private set; }
		public List<Vehicle> Vehicles
		{
			get { return _vehicles; }
		}
		public bool Isset()
		{
			return _vehicles.Count > 0;
		}
		public bool Isset(VehicleType t)
		{
			return _vehicles.Where(q => q.Type == t).Count() > 0;
		}
		public Point Dest
		{
			get { return _dest; }
			set
			{
				_dest = value;
				CheckPoint = MathMiXa.ZeroPoint;
				StartPoint = Center;
				StartTick = MyStrategy.world.TickIndex;
			}
		}
		public Point CheckPoint
		{
			get { return _checkpoint; }
			set { _checkpoint = value; }
		}
		public int TickMoved { get; set; }
		public int DoNotMoveTill { get; set; }
		public Point StartPoint { get; private set; }
		public int StartTick { get; private set; }
		public bool IsGround { get; private set; }
		public bool IsBuilding { get; set; }
		public int ScaledAtTick { get; set; }
		public int RotatedAtTick { get; set; }
	}

	public class MoveItem
	{
		public readonly List<Move> items;
		public readonly int tickToExecute;
		public int tickExecuted;
		private int pointer;

		public MoveItem(int toExecute, params Move[] move)
		{
			items = move.ToList();
			tickToExecute = toExecute;
			pointer = 0;
		}

		public int Count()
		{
			return items.Count;
		}

		public bool Active()
		{
			return Count() > pointer;
		}

		public Move Current()
		{
			return Active() ? items[pointer] : null;
		}

		public Move GetMove()
		{
			var result = Current();
			pointer++;
			return result;
		}

		public bool ExecutionStarted()
		{
			return pointer > 0;
		}
	}

	public class MovesList
	{
		public List<MoveItem> items;
		private int pointer;

		public MovesList()
		{
			items = new List<MoveItem>();
			pointer = 0;
		}

		public void Add(int toExecute, params Move[] move)
		{
			items.Add(new MoveItem(toExecute, move));
		}

		public void Add(params Move[] move)
		{
			Add(0, move);
		}

		public int Count()
		{
			return items.Count;
		}

		public bool Active()
		{
			return Count() > pointer;
		}

		public MoveItem Current()
		{
			return Active() ? items[pointer] : null;
		}

		public Move GetMove(int tick)
		{
			if (!Active())
			{
				return null;
			}
			Reorder(tick);
			Move result = null;
			if (tick >= Current().tickToExecute)
			{
				result = Current().GetMove();
				if (result == null)
				{
					pointer++;
					if (!Active())
					{
						return null;
					}
					result = Current().GetMove();
				}
			}
			if (result != null)
			{
				Current().tickExecuted = tick;
			}
			if (!Current().Active())
			{
				pointer++;
			}
			return result;
		}

		private void Reorder(int tick)
		{
			var i = pointer;
			while (i < Count())
			{
				if (tick >= items[i].tickToExecute)
				{
					break;
				}
				i++;
			}
			if (i < Count() && i > pointer)
			{
				var o = items[i];
				for (var j = i; j > 0; j--)
				{
					items[j] = items[j - 1];
				}
				items[pointer] = o;
			}
		}

		public List<Move> GetFutureMoves()
		{
			return items.Skip(pointer).SelectMany(q => q.items).ToList();
		}

		public bool IssetFutureMoveByType(VehicleType t)
		{
			return GetFutureMoves().Where(q => q.VehicleType == t).Count() > 0;
		}

		public bool IssetFutureMoveByGroup(int g)
		{
			return GetFutureMoves().Where(q => q.Group == g).Count() > 0;
		}

		public int FutureMovesCount { get { return GetFutureMoves().Count(); } }

		public void Clear()
		{
			pointer = Count();
		}

		public void ClearByGroup(int g)
		{
			if (IssetFutureMoveByGroup(g))
			{
				var tp = pointer;
				while (tp < Count())
				{
					if (items[tp].items.Where(q => q.Group == g).Count() > 0)
					{
						items.RemoveAt(tp);
					}
					else
					{
						tp++;
					}
				}
			}
		}

		public void BringToFrontLast(int count = 1)
		{
			var b = pointer;
			if (Current().ExecutionStarted())
			{
				b++;
			}
			var itemsNew = new List<MoveItem>();
			for (var i = Count() - count; i < Count(); i++)
			{
				itemsNew.Add(items[i]);
			}
			for (var i = Count() - count - 1; i >= b; i--)
			{
				items[i + count] = items[i];
			}
			for (var i = 0; i < count; i++)
			{
				items[b + i] = itemsNew[i];
			}
		}
	}

	public struct NuclearPoint
	{
		public Point p;
		public double damage;
		public int Tick { get; }

		public NuclearPoint(Point p, double damage)
		{
			this.p = p;
			this.damage = damage;
			Tick = MyStrategy.world.TickIndex;
		}
	}

	public static class Ext
	{
		public static bool In<T>(this T val, params T[] values) where T : struct
		{
			return values.Contains(val);
		}

		public static bool Eq(this double q, double w)
		{
			return Math.Abs(q - w) < MathMiXa.Eps;
		}
		public static bool More(this double q, double w)
		{
			return q - w > MathMiXa.Eps;
		}
		public static bool MoreEq(this double q, double w)
		{
			return q.More(w) || q.Eq(w);
		}
		public static bool Less(this double q, double w)
		{
			return w.More(q);
		}
		public static bool LessEq(this double q, double w)
		{
			return w.MoreEq(q);
		}
		public static bool Between(this double q, double w, double e)
		{
			return q.More(w) && q.Less(e);
		}
		public static bool BetweenEq(this double q, double w, double e)
		{
			return q.MoreEq(w) && q.LessEq(e);
		}
		public static bool Between(this int q, int w, int e)
		{
			return q > w && q < e;
		}
		public static bool BetweenEq(this int q, int w, int e)
		{
			return q >= w && q <= e;
		}
	}

	public static class MathMiXa
	{
		public static double Eps = 1e-5;
		public static Point ZeroPoint = new Point(0, 0);

		public static double SSignedDoubled(Point p1, Point p2, Point p3)
		{
			return (p2.X - p1.X) * (p3.Y - p1.Y) - (p2.Y - p1.Y) * (p3.X - p1.X);
		}

		public static double Determinant(double a, double b, double c, double d)
		{
			return a * d - b * c;
		}
	}

}